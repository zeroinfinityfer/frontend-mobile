package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class MapEntryModel(

        @SerializedName("key")
        @Expose
        var key: Date? = null,

        @SerializedName("value")
        @Expose
        var value: Double? = null
)
