package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PairModel(
        @SerializedName("date")
        @Expose
        var date: String? = null,
        @SerializedName("value")
        @Expose
        var value: Double? = null
)