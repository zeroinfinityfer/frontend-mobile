package hr.zeroinfinity.models

import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class GoalModel(

        @SerializedName("id")
        @Expose
        var id: Long? = null,

        @SerializedName("name")
        @Expose
        var name: String? = null,

        @SerializedName("description")
        @Expose
        var description: String? = null,

        @SerializedName("targetAmount")
        @Expose
        var targetAmount: BigDecimal? = null,

        @SerializedName("groupId")
        @Expose
        var groupId: UUID? = null,

        @SerializedName("targetDate")
        @Expose
        var targetDate: Date? = null
) : Parcelable {
    constructor() : this(null, null, null, null, null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GoalModel

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<GoalModel> = object : DiffUtil.ItemCallback<GoalModel>() {

            override fun areItemsTheSame(oldItem: GoalModel, newItem: GoalModel): Boolean {
                return areContentsTheSame(oldItem, newItem)
            }

            override fun areContentsTheSame(oldItem: GoalModel, newItem: GoalModel): Boolean {
                return when {
                    oldItem.targetAmount != newItem.targetAmount -> false
                    oldItem.description != newItem.description -> false
                    oldItem.groupId != newItem.groupId -> false
                    oldItem.id != newItem.id -> false
                    oldItem.name != newItem.name -> false
                    oldItem.targetDate != newItem.targetDate -> false
                    else -> true
                }
            }
        }
    }
}
