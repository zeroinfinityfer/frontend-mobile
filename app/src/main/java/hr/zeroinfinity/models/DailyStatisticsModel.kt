package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class DailyStatisticsModel(
        @SerializedName("incomes")
        @Expose
        var incomes: List<IncomeModel>? = null,

        @SerializedName("expenses")
        @Expose
        var expenses: List<ExpenseModel>? = null,

        @SerializedName("goals")
        @Expose
        var goals: List<GoalModel>? = null
)
