package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class MemberInviteModel(

        @SerializedName("id")
        @Expose
        val id: UUID,

        @SerializedName("memberEmail")
        @Expose
        val memberEmail: String)
