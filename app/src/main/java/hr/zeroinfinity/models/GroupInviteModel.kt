package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class GroupInviteModel(

        @SerializedName("id")
        @Expose
        val id: UUID,

        @SerializedName("members")
        @Expose
        val members: MutableList<String>
)
