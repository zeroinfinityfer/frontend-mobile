package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.*

data class EntryModel(

        @SerializedName("title")
        @Expose
        var title: String? = null,

        @SerializedName("type")
        @Expose
        var type: String? = null,

        @SerializedName("amount")
        @Expose
        var amount: BigDecimal? = null,

        @SerializedName("dateCreated")
        @Expose
        var dateCreated: Date? = null,

        @SerializedName("category")
        @Expose
        var category: String? = null,

        @SerializedName("description")
        @Expose
        var description: String? = null


) {
    constructor() : this(null, null, null, null, null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        return true
    }

}
