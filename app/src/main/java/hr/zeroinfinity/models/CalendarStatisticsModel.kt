package hr.zeroinfinity.models

data class CalendarStatisticsModel(

        val dailyBalance: List<PairModel>?,

        val goals: List<PairModel>?,

        val savings: List<PairModel>?
)