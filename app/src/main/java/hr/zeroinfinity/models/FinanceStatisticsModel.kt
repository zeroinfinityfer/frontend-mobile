package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FinanceStatisticsModel(
        @SerializedName("lineChartData")
        @Expose
        var lineChartData: MutableList<PairModel>? = null,
        @SerializedName("barChartData")
        @Expose
        var barChartData: MutableList<PairModel>? = null
)
