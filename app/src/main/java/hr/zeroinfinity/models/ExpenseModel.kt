package hr.zeroinfinity.models

import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class ExpenseModel(

        @SerializedName("id")
        @Expose
        var id: Long? = null,

        @SerializedName("amount")
        @Expose
        var amount: BigDecimal? = null,

        @SerializedName("description")
        @Expose
        var description: String? = null,

        @SerializedName("dateCreated")
        @Expose
        var dateCreated: Date? = null,

        @SerializedName("groupId")
        @Expose
        var groupId: UUID? = null,

        @SerializedName("categoryId")
        @Expose
        var categoryId: Long? = null
) : Parcelable {
    constructor() : this(null, null, null, null, null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExpenseModel

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<ExpenseModel> = object : DiffUtil.ItemCallback<ExpenseModel>() {

            override fun areItemsTheSame(oldItem: ExpenseModel, newItem: ExpenseModel): Boolean {
                return areContentsTheSame(oldItem, newItem)
            }

            override fun areContentsTheSame(oldItem: ExpenseModel, newItem: ExpenseModel): Boolean {
                return when {
                    oldItem.id != newItem.id -> false
                    oldItem.amount != newItem.amount -> false
                    oldItem.categoryId != newItem.categoryId -> false
                    oldItem.dateCreated != newItem.dateCreated -> false
                    oldItem.description != newItem.description -> false
                    oldItem.groupId != newItem.groupId -> false
                    else -> true
                }

            }
        }
    }
}
