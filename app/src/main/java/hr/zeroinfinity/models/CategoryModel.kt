package hr.zeroinfinity.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class CategoryModel(

        @SerializedName("id")
        @Expose
        var id: Long? = null,

        @SerializedName("name")
        @Expose
        var name: String? = null,

        @SerializedName("groupId")
        @Expose
        var groupId: UUID? = null,

        @IgnoredOnParcel
        var color: Int? = null
) : Parcelable {
    constructor() : this(null, null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CategoryModel

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }


}
