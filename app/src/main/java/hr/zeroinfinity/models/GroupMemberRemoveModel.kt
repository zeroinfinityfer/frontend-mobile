package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class GroupMemberRemoveModel(
        @SerializedName("userId")
        @Expose
        var userId: UUID,
        @SerializedName("groupId")
        @Expose
        var groupId: UUID
)