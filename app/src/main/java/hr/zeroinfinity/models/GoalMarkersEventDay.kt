package hr.zeroinfinity.models

import com.applandeo.materialcalendarview.EventDay
import hr.zeroinfinity.R
import java.text.SimpleDateFormat
import java.util.*

class GoalMarkersEventDay(goalData: PairModel) : EventDay(resolveCalendar(goalData), R.drawable.star) {
    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        private fun resolveCalendar(financeStats: PairModel): Calendar {
            val calendar = Calendar.getInstance()
            calendar.time = DATE_FORMAT.parse(financeStats.date)

            return calendar
        }
    }
}