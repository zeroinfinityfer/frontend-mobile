package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class GroupAdminModel(

        @SerializedName("groupId")
        @Expose
        val groupId: UUID,

        @SerializedName("userId")
        @Expose
        val userId: UUID)
