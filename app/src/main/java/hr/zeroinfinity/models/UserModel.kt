package hr.zeroinfinity.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class UserModel(

        @SerializedName("id")
        @Expose
        var id: UUID? = null,

        @SerializedName("name")
        @Expose
        var name: String? = null,

        @SerializedName("surname")
        @Expose
        var surname: String? = null,

        @SerializedName("email")
        @Expose
        var email: String? = null,

        @SerializedName("password")
        @Expose
        var password: String? = null,

        @Expose(serialize = false)
        var confirmPassword: String? = null,

        @SerializedName("personalGroupEnabled")
        @Expose
        var personalGroupEnabled: Boolean? = null,

        @SerializedName("currency")
        @Expose
        var currency: String? = null,

        @Expose(serialize = false)
        var isGroupAdmin: Boolean? = null) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserModel

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }
}