package hr.zeroinfinity.models

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import com.applandeo.materialcalendarview.CalendarUtils
import com.applandeo.materialcalendarview.EventDay
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.absoluteValue

class FinanceStatsEventDay(context: Context, financeStats: PairModel) : EventDay(resolveCalendar(financeStats), resolveDrawable(context, financeStats)) {
    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        private fun resolveCalendar(financeStats: PairModel): Calendar {
            val calendar = Calendar.getInstance()
            calendar.time = DATE_FORMAT.parse(financeStats.date)

            return calendar
        }

        private fun resolveDrawable(context: Context, financeStats: PairModel): Drawable {
            val text: String
            val color: Int

            if(financeStats.value!! > 0) {
                text = String.format("+%.2f", financeStats.value)
                color = android.R.color.holo_green_light
            } else {
                text = String.format("%.2f", financeStats.value)
                color = android.R.color.holo_red_light
            }

            val absValue = financeStats.value?.absoluteValue!!
            val size = if(absValue < 100){
                6
            } else if(absValue >= 100 || absValue < 10000){
                5
            } else {
                4
            }

            return CalendarUtils.getDrawableText(
                    context,
                    text,
                    Typeface.DEFAULT,
                    color,
                    size)
        }
    }
}