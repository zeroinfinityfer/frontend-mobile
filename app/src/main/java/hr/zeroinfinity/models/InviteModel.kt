package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class InviteModel(

        @SerializedName("id")
        @Expose
        var id: Long?,

        @SerializedName("groupId")
        @Expose
        var groupId: UUID?,

        @SerializedName("email")
        @Expose
        var email: String?,

        @SerializedName("createdAt")
        @Expose
        var createdAt: Date,

        @SerializedName("groupName")
        @Expose
        var groupName: String?,

        @SerializedName("invitedBy")
        @Expose(serialize = false)
        var invitedBy: UserModel?)