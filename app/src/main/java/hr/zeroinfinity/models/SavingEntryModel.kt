package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.*

data class SavingEntryModel(
        @SerializedName("groupId")
        @Expose
        var groupId: UUID,

        @SerializedName("amountAdded")
        @Expose
        var amountAdded: BigDecimal,

        @SerializedName("dateCreated")
        @Expose
        var dateCreated: Date
)