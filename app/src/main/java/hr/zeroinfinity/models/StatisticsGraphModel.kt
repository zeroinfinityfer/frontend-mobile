package hr.zeroinfinity.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class StatisticsGraphModel(

        @SerializedName("unit")
        @Expose
        var unit: String? = null,

        @SerializedName("unitLength")
        @Expose
        var unitLength: Int? = null,

        @SerializedName("maxAmount")
        @Expose
        var maxAmount: Double? = null,

        @SerializedName("minAmount")
        @Expose
        var minAmount: Double? = null,

        @SerializedName("markers")
        @Expose
        var markers: MutableList<Pair<Date, Double>>? = null
)
