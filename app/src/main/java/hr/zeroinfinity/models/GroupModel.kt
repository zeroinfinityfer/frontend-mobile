package hr.zeroinfinity.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class GroupModel(

        @SerializedName("id")
        @Expose
        var id: UUID? = null,

        @SerializedName("name")
        @Expose
        var name: String? = null,

        @SerializedName("description")
        @Expose
        var description: String? = null,

        @SerializedName("members")
        @Expose
        var members: MutableList<UserModel>? = null,

        @SerializedName("admins")
        @Expose
        var admins: MutableList<UserModel>? = null,

        @SerializedName("invitationEmails")
        @Expose
        var invitationEmails: MutableList<String>? = null,

        @SerializedName("personal")
        @Expose
        var personal: Boolean? = null,

        @SerializedName("categories")
        var categories: MutableList<CategoryModel>? = null,

        @SerializedName("currency")
        @Expose
        var currency: String? = null,

        @SerializedName("savingsAmount")
        var savingsAmount: BigDecimal? = null,

        @Expose(serialize = false)
        var activeUserIsAdmin: Boolean = false

) : Parcelable {

    constructor() : this(null, null, null, null, null, null, null, null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GroupModel

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(id!!)
    }


}
