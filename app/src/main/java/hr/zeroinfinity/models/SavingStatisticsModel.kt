package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SavingStatisticsModel(
        @SerializedName("goals")
        @Expose
        var goals: List<GoalModel>? = null,

        @SerializedName("savings")
        @Expose
        var savings: List<PairModel>? = null
)