package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeviceSubscriptionModel(

        @SerializedName("oldToken")
        @Expose
        val oldToken: String?,

        @SerializedName("newToken")
        @Expose
        val newToken: String
)
