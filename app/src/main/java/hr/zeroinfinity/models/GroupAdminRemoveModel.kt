package hr.zeroinfinity.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class GroupAdminRemoveModel(
        @SerializedName("groupId")
        @Expose
        var groupId: UUID
)