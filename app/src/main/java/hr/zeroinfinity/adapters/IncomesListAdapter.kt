package hr.zeroinfinity.adapters

import android.app.Activity
import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.IncomeUpdateActivity
import hr.zeroinfinity.activities.IncomesTabFragment
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.IncomeModel
import hr.zeroinfinity.models.PairModel
import hr.zeroinfinity.services.ColorResolver
import hr.zeroinfinity.services.XAxisValueResolver
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*


class IncomesListAdapter(var group: GroupModel?) : PagedListAdapter<IncomeModel, RecyclerView.ViewHolder>(IncomeModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_income, parent, false)
        return IncomesListViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as IncomesListViewHolder).bindTo(getItem(position))
    }

    inner class IncomesListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val date: TextView = itemView.findViewById(R.id.income_date)
        private val amount: TextView = itemView.findViewById(R.id.income_amount)
        private val description: TextView = itemView.findViewById(R.id.income_description)

        fun bindTo(incomeModel: IncomeModel?) {
            if (incomeModel != null) {
                date.text = DATE_FORMAT.format(incomeModel.dateCreated)
                amount.text = String.format("%s %s", incomeModel.amount, group?.currency)
                description.text = incomeModel.description

                itemView.findViewById<ImageView>(R.id.edit_income_btn).setOnClickListener {
                    val i = Intent(itemView.context, IncomeUpdateActivity::class.java)

                    i.putExtra("model", incomeModel)

                    (itemView.context as Activity).startActivityForResult(i, IncomesTabFragment.updateIncomeRequestCode)
                }
            }
        }
    }

    inner class IncomesListViewHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var progressBar: ProgressBar
        private lateinit var barChart: BarChart
        private lateinit var textView: TextView

        fun bindTo() {
            barChart = itemView.findViewById(R.id.income_chart)
            progressBar = itemView.findViewById(R.id.progressBar_cyclic)
            textView = itemView.findViewById(R.id.no_data_text)

            barChart.description?.isEnabled = false
            barChart.setMaxVisibleValueCount(366)
            barChart.setPinchZoom(false)

            barChart.setDrawBarShadow(false)
            barChart.setDrawGridBackground(false)

            val xAxis = barChart.xAxis
            xAxis?.position = XAxis.XAxisPosition.BOTTOM
            xAxis?.setDrawGridLines(false)
            xAxis?.textColor = Color.WHITE

            barChart.axisLeft?.setDrawGridLines(true)
            barChart.legend?.isEnabled = false
            barChart.axisLeft?.axisMinimum = 0f
            barChart.axisRight?.isEnabled = false
            barChart.xAxis?.isGranularityEnabled = true
            barChart.xAxis?.textSize = 6f
            barChart.axisLeft?.textColor = Color.WHITE

            val spinner: Spinner = itemView.findViewById(R.id.period_spinner)
            ArrayAdapter.createFromResource(
                    itemView.context,
                    R.array.time_period_array,
                    android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
                spinner.setSelection(when (timeUnit) {
                    "d" -> 0
                    "m" -> 1
                    "y" -> 2
                    else -> 0
                })
            }

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    timeUnit = when (position) {
                        0 -> "d"
                        1 -> "m"
                        2 -> "y"
                        else -> "d"
                    }

                    if (group != null) fetchData()
                }
            }
        }

        fun fetchData() {
            progressBar.visibility = View.VISIBLE
            val periodLength: Int = when (timeUnit) {
                "m" -> 1
                "d" -> 7
                "y" -> 1
                else -> 30
            }

            FinanceAPI.instance.getIncomesStatistics(group?.id!!, timeUnit, periodLength, DATE_FORMAT.format(Date()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                setData(it)
                                progressBar.visibility = View.GONE
                            },
                            {
                                Log.e("HTTP ERROR", it.message)
                                progressBar.visibility = View.GONE
                            })
        }

        private fun setData(incomes: List<PairModel>) {
            val values = ArrayList<BarEntry>()
            var index = 0
            var maxY = 0f

            for (pair in incomes) {
                val value = pair.value!!.toFloat()
                if (value > maxY) {
                    maxY = value
                }

                val barEntry = BarEntry(index.toFloat(), value)
                barEntry.data = pair.date
                values.add(barEntry)
                index += 1
            }

            if (maxY == 0f) {
                barChart.visibility = View.GONE
                textView.visibility = View.VISIBLE
                return
            } else {
                barChart.visibility = View.VISIBLE
                textView.visibility = View.GONE
            }

            val xAxisValues = XAxisValueResolver.resolveValues(incomes)
            barChart.xAxis.valueFormatter = IndexAxisValueFormatter(xAxisValues)


            val dataSet: BarDataSet
            barChart.axisLeft?.axisMaximum = maxY + (maxY * 0.1).toFloat()
            barChart.xAxis?.setLabelCount(values.size, false)
            barChart.xAxis?.granularity = when (timeUnit) {
                "m" -> 4f
                else -> 1f
            }

            if (barChart.data != null && barChart.data.dataSetCount > 0) {
                dataSet = barChart.data.getDataSetByIndex(0) as BarDataSet
                dataSet.values = values
                dataSet.colors = ColorResolver.resolveValues(values.size)
                barChart.data.notifyDataChanged()
                barChart.notifyDataSetChanged()
            } else {
                dataSet = BarDataSet(values, "")
                dataSet.colors = ColorResolver.resolveValues(values.size)
                dataSet.setDrawValues(false)

                val dataSets = ArrayList<IBarDataSet>()
                dataSets.add(dataSet)

                val data = BarData(dataSets)
                barChart.data = data
                barChart.setFitBars(true)
            }

            barChart.invalidate()
            barChart.animateY(1000)
        }
    }

    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        private var timeUnit: String = "d"
    }
}