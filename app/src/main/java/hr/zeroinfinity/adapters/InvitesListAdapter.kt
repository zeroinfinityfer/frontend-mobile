package hr.zeroinfinity.adapters

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.common.Listener
import hr.zeroinfinity.models.InviteModel
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InvitesListAdapter(context: Context, resource: Int, private val objects: MutableLiveData<MutableList<InviteModel>>) : ArrayAdapter<InviteModel>(context, resource, objects.value
        ?: mutableListOf<InviteModel>()) {

    private var listener: Listener<Boolean>? = null

    fun setOnHandleInviteListener(_listener: Listener<Boolean>){
        listener = _listener
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.invites_list_item, parent, false)
        }

        val invite = getItem(position)

        view?.findViewById<TextView>(R.id.group_name_text_view)?.text = invite?.groupName.toString()
        view?.findViewById<TextView>(R.id.invited_by_text_view)?.text = " ${invite?.invitedBy?.name} ${invite?.invitedBy?.surname}, ${invite?.invitedBy?.email}"
        view?.findViewById<Button>(R.id.accept_button)?.setOnClickListener {
            GroupAPI.instance.handleInvite(invite.id!!, true)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                objects.value?.removeAt(position)
                                notifyDataSetChanged()
                                objects.notifyObserver()
                                listener?.update(true)
                            },
                            {
                                Log.e("HTTP ERROR", it.message)
                                Toast.makeText(context, context.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                            })
        }

        view?.findViewById<Button>(R.id.decline_button)?.setOnClickListener {
            GroupAPI.instance.handleInvite(invite.id!!, false)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                objects.value?.removeAt(position)
                                notifyDataSetChanged()
                                objects.notifyObserver()
                                listener?.update(false)
                            },
                            {
                                Log.e("HTTP ERROR", it.message)
                                Toast.makeText(context, context.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                            })
        }

        return view!!
    }

    private fun <T> MutableLiveData<T>.notifyObserver() {
        this.value = this.value
    }
}