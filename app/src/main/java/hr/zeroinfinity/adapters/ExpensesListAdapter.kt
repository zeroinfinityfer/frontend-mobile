package hr.zeroinfinity.adapters

import android.app.Activity
import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.ExpenseUpdateActivity
import hr.zeroinfinity.activities.ExpensesTabFragment
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class ExpensesListAdapter(var categories: MutableMap<Long, CategoryModel>, var group: GroupModel?) : PagedListAdapter<ExpenseModel, RecyclerView.ViewHolder>(ExpenseModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_expense, parent, false)
        return ExpenseListViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ExpenseListViewHolder).bindTo(getItem(position))
    }

    inner class ExpenseListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val date: TextView = itemView.findViewById(R.id.expense_date)
        private val category: TextView = itemView.findViewById(R.id.expense_category)
        private val description: TextView = itemView.findViewById(R.id.expense_description)
        private val amount: TextView = itemView.findViewById(R.id.expense_amount)

        fun bindTo(expenseModel: ExpenseModel?) {
            if (expenseModel != null) {

                date.text = DATE_FORMAT.format(expenseModel.dateCreated)
                description.text = expenseModel.description
                category.text = categories[expenseModel.categoryId]!!.name
                category.background.colorFilter = PorterDuffColorFilter(categories[expenseModel.categoryId]?.color!!, PorterDuff.Mode.SRC_IN)
                amount.text = String.format("%s %s", expenseModel.amount, group?.currency)

                itemView.findViewById<ImageView>(R.id.edit_expense_btn).setOnClickListener {
                    val i = Intent(itemView.context, ExpenseUpdateActivity::class.java)

                    i.putParcelableArrayListExtra("categories", ArrayList(categories.values))
                    i.putExtra("model", expenseModel)

                    (itemView.context as Activity).startActivityForResult(i, ExpensesTabFragment.updateExpenseRequestCode)
                }
            }

        }
    }

    inner class ExpenseListViewHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var pieChart: PieChart
        private lateinit var progressBar: ProgressBar
        private lateinit var textView: TextView

        fun bindTo() {
            progressBar = itemView.findViewById(R.id.progressBar_cyclic)
            pieChart = itemView.findViewById(R.id.expense_chart)
            textView = itemView.findViewById(R.id.no_data_text)

            pieChart.setNoDataText(itemView.context.getString(R.string.no_data_label))
            pieChart.setUsePercentValues(true)
            pieChart.description?.isEnabled = false
            pieChart.setExtraOffsets(5f, 10f, 5f, 5f)
            pieChart.dragDecelerationFrictionCoef = 0.95f

            pieChart.setHoleColor(itemView.context.getColor(R.color.half_black))
            pieChart.setTransparentCircleColor(Color.LTGRAY)
            pieChart.setTransparentCircleAlpha(110)
            pieChart.holeRadius = 38f
            pieChart.transparentCircleRadius = 43f

            pieChart.isDrawHoleEnabled = true
            pieChart.setDrawCenterText(true)
            pieChart.legend?.isEnabled = false

            pieChart.rotationAngle = 0f
            pieChart.isRotationEnabled = true
            pieChart.isHighlightPerTapEnabled = true

            val spinner: Spinner = itemView.findViewById(R.id.period_spinner)
            ArrayAdapter.createFromResource(
                    itemView.context,
                    R.array.time_period_array,
                    android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
                spinner.setSelection(when (timeUnit) {
                    "d" -> 0
                    "m" -> 1
                    "y" -> 2
                    else -> 0
                })
            }

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    timeUnit = when (position) {
                        0 -> "d"
                        1 -> "m"
                        2 -> "y"
                        else -> "d"
                    }

                    if (group != null) fetchData()
                }
            }
        }

        fun fetchData() {
            progressBar.visibility = View.VISIBLE
            val periodLength: Int = when (timeUnit) {
                "m" -> 1
                "d" -> 7
                "y" -> 1
                else -> 30
            }

            FinanceAPI.instance.getExpensesStatistics(group?.id!!, timeUnit, periodLength, DATE_FORMAT.format(Date()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                setData(it)
                                progressBar.visibility = View.GONE
                            },
                            {
                                Log.e("HTTP ERROR", it.message)
                                progressBar.visibility = View.GONE
                            })
        }

        private fun setData(_categories: Map<String, Double>) {
            if (_categories.isEmpty()) {
                pieChart.isDrawHoleEnabled = false
                pieChart.setDrawCenterText(false)
                textView.visibility = View.VISIBLE
                pieChart.visibility = View.GONE
            } else {
                textView.visibility = View.GONE
                pieChart.visibility = View.VISIBLE
                pieChart.isDrawHoleEnabled = true
                pieChart.setDrawCenterText(true)
            }

            val values = mutableListOf<PieEntry>()
            for (cat in _categories.keys) {
                values.add(PieEntry(_categories[cat]?.toFloat()!!, cat))
            }


            val dataSet = PieDataSet(values, "")
            dataSet.sliceSpace = 1f
            dataSet.selectionShift = 5f
            dataSet.colors = resolveColors(_categories)
            dataSet.valueTextColor = R.color.white

            val data = PieData(dataSet)
            data.setValueTextSize(10f)
            data.setValueTextColor(Color.WHITE)

            pieChart.data = data
            pieChart.animateY(2000, Easing.EaseInOutCubic)
        }

        private fun resolveColors(_categories: Map<String, Double>): MutableList<Int> {
            val models = categories.values
            val result: MutableList<Int> = mutableListOf()

            for (name in _categories.keys) {
                for (model in models) {
                    if (name == model.name) {
                        result.add(model.color!!)
                        break
                    }
                }
            }

            return result
        }
    }

    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        private var timeUnit: String = "d"
        private const val TYPE_HEADER = 0
    }
}