package hr.zeroinfinity.adapters

import android.app.Activity
import android.content.Intent
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.GoalUpdateActivity
import hr.zeroinfinity.activities.StatisticActivity
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.models.GroupModel

class DailyGoalsListAdapter(private val group: GroupModel) : ListAdapter<GoalModel, DailyGoalsListAdapter.DailyGoalViewHolder>(GoalModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyGoalViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_goal_daily, parent, false)
        return DailyGoalViewHolder(view)
    }

    override fun onBindViewHolder(p0: DailyGoalViewHolder, p1: Int) {
        p0.bind(getItem(p1), group)
    }

    inner class DailyGoalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(goal: GoalModel, group: GroupModel) {
            val amount = itemView.findViewById<TextView>(R.id.goal_amount)
            val description = itemView.findViewById<TextView>(R.id.goal_description)
            val name = itemView.findViewById<TextView>(R.id.goal_name)

            amount?.text = "${goal?.targetAmount} ${group.currency}"
            description?.text = goal.description
            name?.text = goal.name

            itemView.setOnLongClickListener {
                val context = (itemView.parent as ViewGroup).context
                val i = Intent(context, GoalUpdateActivity::class.java)

                i.putExtra("model", goal)

                (context as Activity).startActivityForResult(i, StatisticActivity.REQUEST_CODE)

                return@setOnLongClickListener true
            }

        }
    }
}