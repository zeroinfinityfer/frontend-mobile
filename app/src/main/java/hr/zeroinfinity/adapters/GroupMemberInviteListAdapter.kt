package hr.zeroinfinity.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import hr.zeroinfinity.R

class GroupMemberInviteListAdapter : ArrayAdapter<String> {

    private var mCtx: Context = context
    private var resource: Int = 0
    private var userEmailList: MutableList<String>

    constructor(mCtx: Context, resource: Int, userEmailList: MutableList<String>) : super(mCtx, resource, userEmailList) {
        this.resource = resource
        this.userEmailList = userEmailList


    }

    /**
     * @var view represents single row in our table which contains user_email(text_view) and button to remove
     *      row.
     * */

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resource, null)

        val userEmail: TextView = view.findViewById(R.id.user_email)
        val removeButton: Button = view.findViewById(R.id.remove_button)

        userEmail.text = userEmailList.get(position)

        removeButton.setOnClickListener {
            userEmailList.removeAt(position)
            notifyDataSetChanged()
        }

        return view
    }
}