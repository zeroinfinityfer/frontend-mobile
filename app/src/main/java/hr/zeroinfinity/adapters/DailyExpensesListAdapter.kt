package hr.zeroinfinity.adapters

import android.app.Activity
import android.content.Intent
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.ExpenseUpdateActivity
import hr.zeroinfinity.activities.StatisticActivity
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.models.GroupModel

class DailyExpensesListAdapter(private val group: GroupModel) : ListAdapter<ExpenseModel, DailyExpensesListAdapter.DailyExpenseViewHolder>(ExpenseModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyExpenseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_expense_daily, parent, false)
        return DailyExpenseViewHolder(view)
    }

    override fun onBindViewHolder(p0: DailyExpenseViewHolder, p1: Int) {
        p0.bind(getItem(p1), group)
    }

    inner class DailyExpenseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(expense: ExpenseModel, group: GroupModel) {
            val amount = itemView.findViewById<TextView>(R.id.expense_amount)
            val description = itemView.findViewById<TextView>(R.id.expense_description)
            val category = itemView.findViewById<TextView>(R.id.expense_category)

            amount?.text = "${expense?.amount} ${group.currency}"
            description?.text = expense.description
            category?.text = group.categories?.firstOrNull { it.id == expense.categoryId }?.name
            itemView?.setOnLongClickListener {
                val context = (itemView.parent as ViewGroup).context
                val i = Intent(context, ExpenseUpdateActivity::class.java)

                i.putParcelableArrayListExtra("categories", ArrayList(group.categories!!))
                i.putExtra("model", expense)

                (context as Activity).startActivityForResult(i, StatisticActivity.REQUEST_CODE)

                return@setOnLongClickListener true
            }

        }
    }
}