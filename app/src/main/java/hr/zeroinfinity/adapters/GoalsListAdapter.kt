package hr.zeroinfinity.adapters

import android.app.Activity
import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.graphics.Color
import android.graphics.DashPathEffect
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.GoalUpdateActivity
import hr.zeroinfinity.activities.GoalsTabFragment
import hr.zeroinfinity.common.MyMarkerView
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.PairModel
import hr.zeroinfinity.services.ColorResolver
import hr.zeroinfinity.services.XAxisValueResolver
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.function.Consumer

class GoalsListAdapter(var group: GroupModel?, val refreshCallback: Consumer<Boolean>) : PagedListAdapter<GoalModel, RecyclerView.ViewHolder>(GoalModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_goal, parent, false)
        return GoalListViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GoalListViewHolder).bindTo(getItem(position))
    }

    inner class GoalListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val date: TextView = itemView.findViewById(R.id.goal_target_date)
        private val name: TextView = itemView.findViewById(R.id.goal_name)
        private val amount: TextView = itemView.findViewById(R.id.goal_amount)

        fun bindTo(goalModel: GoalModel?) {
            if (goalModel != null) {
                date.text = DATE_FORMAT.format(goalModel.targetDate)
                name.text = goalModel.name
                amount.text = String.format("%s %s", goalModel.targetAmount, group?.currency)

                itemView.findViewById<ImageView>(R.id.edit_goal_btn).setOnClickListener {
                    val i = Intent(itemView.context, GoalUpdateActivity::class.java)

                    i.putExtra("model", goalModel)

                    (itemView.context as Activity).startActivityForResult(i, GoalsTabFragment.updateGoalRequestCode)
                }

                itemView.setOnLongClickListener {
                    if (group?.savingsAmount?.compareTo(goalModel.targetAmount)!! >= 0) {
                        AlertDialog.Builder(itemView.context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle(itemView.context.getString(R.string.goal_approve_label))
                                .setMessage(itemView.context.getString(R.string.are_you_sure_label))
                                .setPositiveButton(itemView.context.getString(R.string.yes)) { _, _ ->
                                    run {
                                        FinanceAPI.instance.approveGoal(goalModel.id!!, DATE_FORMAT.format(Date()))
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(
                                                        {
                                                            refreshCallback.accept(true)
                                                        },
                                                        {
                                                            Log.e("HTTP ERROR", it.message)
                                                            Toast.makeText(itemView.context, itemView.context.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                                        }
                                                )
                                    }
                                }.setNegativeButton(itemView.context.getString(R.string.no), null)
                                .show()
                    } else {
                        Toast.makeText(itemView.context, itemView.context.getString(R.string.need_cash), Toast.LENGTH_SHORT).show()

                    }

                    return@setOnLongClickListener true
                }
            }
        }
    }

    inner class GoalsListViewHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var lineChart: LineChart
        private lateinit var progressBar: ProgressBar
        private lateinit var textView: TextView

        fun bindTo() {
            lineChart = itemView.findViewById(R.id.savings_chart)
            textView = itemView.findViewById(R.id.no_data_text)

            lineChart.setDrawGridBackground(false)
            lineChart.description?.isEnabled = false
            lineChart.setTouchEnabled(true)
            lineChart.setDrawGridBackground(false)
            lineChart.isDragEnabled = true
            lineChart.setScaleEnabled(true)
            lineChart.setPinchZoom(true)
            lineChart.legend.isEnabled = false

            lineChart.xAxis.enableGridDashedLine(0f, 100f, 0f)
            lineChart.xAxis.isGranularityEnabled = true
            lineChart.xAxis.textSize = 6f
            lineChart.xAxis.textColor = Color.WHITE
            lineChart.xAxis.position = XAxis.XAxisPosition.BOTTOM

            lineChart.axisLeft!!.setDrawLimitLinesBehindData(true)
            lineChart.xAxis.setDrawLimitLinesBehindData(true)
            lineChart.axisLeft?.axisMinimum = 0f
            lineChart.axisRight?.isEnabled = false
            lineChart.axisLeft?.textColor = Color.WHITE

            val mv = MyMarkerView(itemView.context, R.layout.custom_marker_view)
            mv.chartView = lineChart
            lineChart.marker = mv

            progressBar = itemView.findViewById(R.id.progressBar_cyclic)
            val spinner: Spinner = itemView.findViewById(R.id.period_spinner)
            ArrayAdapter.createFromResource(
                    itemView.context,
                    R.array.time_period_array,
                    android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
                spinner.setSelection(when (timeUnit) {
                    "d" -> 0
                    "m" -> 1
                    "y" -> 2
                    else -> 0
                })
            }

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    timeUnit = when (position) {
                        0 -> "d"
                        1 -> "m"
                        2 -> "y"
                        else -> "d"
                    }

                    fetchData()
                }
            }
        }

        fun fetchData() {
            progressBar.visibility = View.VISIBLE
            val periodLength: Int = when (timeUnit) {
                "m" -> 1
                "d" -> 7
                "y" -> 1
                else -> 30
            }

            FinanceAPI.instance.getSavingsStatistics(group?.id!!, timeUnit, periodLength, DATE_FORMAT.format(Date()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val savings = it.savings
                                val goals = it.goals

                                if (savings != null && goals != null) {
                                    setData(savings, goals)
                                } else {
                                    lineChart.data?.clearValues()
                                    lineChart.invalidate()
                                }

                                progressBar.visibility = View.GONE
                            },
                            {
                                Log.e("HTTP ERROR", it.message)
                                progressBar.visibility = View.GONE
                            })
        }

        private fun setData(savings: List<PairModel>, goals: List<GoalModel>) {
            val values = ArrayList<Entry>()
            var maxY = 0f
            var index = 0
            for (it in savings) {
                val value = it.value!!.toFloat()
                if (value > maxY) {
                    maxY = value
                }

                val barEntry = BarEntry(index.toFloat(), value)
                barEntry.data = it.date
                values.add(barEntry)
                index += 1
            }

            if (maxY == 0f) {
                lineChart.visibility = View.GONE
                textView.visibility = View.VISIBLE
                return
            } else {
                lineChart.visibility = View.VISIBLE
                textView.visibility = View.GONE
            }

            val xAxisValues = XAxisValueResolver.resolveValues(savings)
            val xAxis: XAxis = lineChart.xAxis!!
            xAxis.valueFormatter = IndexAxisValueFormatter(xAxisValues)
            xAxis.granularity = when (timeUnit) {
                "m" -> 4f
                else -> 1f
            }

            val yAxis: YAxis = lineChart.axisLeft!!
            yAxis.limitLines.clear()

            for (i in 0 until goals.size) {
                if (goals[i].targetAmount?.toDouble()?.toFloat()!! > maxY) {
                    continue
                }

                val ll = LimitLine(goals[i].targetAmount!!.toDouble().toFloat(), goals[i].name)
                ll.lineWidth = 2f
                ll.enableDashedLine(10f, 6f, 0f)
                ll.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
                ll.textSize = 10f
                ll.textColor = Color.WHITE
                ll.lineColor = ColorResolver.getRandomColor()
                yAxis.addLimitLine(ll)
            }

            yAxis.axisMaximum = maxY + (maxY * 0.1).toFloat()
            val dataSet: LineDataSet

            if (lineChart.data != null && lineChart.data!!.dataSetCount > 0) {
                dataSet = lineChart.data!!.getDataSetByIndex(0) as LineDataSet
                dataSet.values = values
                dataSet.color = ColorResolver.getRandomColor()
                dataSet.fillColor = ColorResolver.getRandomColor()
                dataSet.valueTextColor = Color.WHITE

                lineChart.data.notifyDataChanged()
                lineChart.notifyDataSetChanged()
            } else {
                dataSet = LineDataSet(values, "")
                dataSet.color = ColorResolver.getRandomColor()
                dataSet.setDrawIcons(false)
                dataSet.enableDashedLine(10f, 5f, 0f)
                dataSet.setCircleColor(ColorResolver.getRandomColor())
                dataSet.lineWidth = 1f
                dataSet.circleRadius = 3f
                dataSet.setDrawCircleHole(false)
                dataSet.formLineWidth = 1f
                dataSet.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
                dataSet.formSize = 15f
                dataSet.valueTextSize = 9f
                dataSet.enableDashedHighlightLine(10f, 5f, 0f)
                dataSet.setDrawFilled(true)
                dataSet.fillColor = ColorResolver.getRandomColor()
                dataSet.fillFormatter = IFillFormatter { _, _ -> lineChart.axisLeft?.axisMinimum!! }

                val dataSets = ArrayList<ILineDataSet>()
                dataSets.add(dataSet)
                lineChart.data = LineData(dataSets)
            }

            lineChart.invalidate()
            lineChart.animateY(1500)
        }
    }

    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        private var timeUnit: String = "d"
    }
}