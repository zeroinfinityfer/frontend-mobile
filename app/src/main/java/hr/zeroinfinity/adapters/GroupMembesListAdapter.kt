package hr.zeroinfinity.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.models.GroupAdminModel
import hr.zeroinfinity.models.GroupMemberRemoveModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.UserModel
import hr.zeroinfinity.services.UserService
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GroupMembesListAdapter(context: Context, resource: Int,
                             private val group: GroupModel)
    : ArrayAdapter<UserModel>(context, resource, group.members) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.members_list_item, parent, false)
        }


        val setAsAdminBtn = view?.findViewById<Button>(R.id.set_as_admin_button)
        val removeBtn = view?.findViewById<Button>(R.id.remove_button)
        val nameDisplay = view?.findViewById<TextView>(R.id.user_name)
        val user = getItem(position)

        nameDisplay?.text = "${user.name} ${user.surname}"
        nameDisplay?.visibility = View.VISIBLE
        val currentUserId = UserService.getUserId()
        if (group.activeUserIsAdmin && user.id != currentUserId) {
            if (!user.isGroupAdmin!!) {
                setAsAdminBtn?.visibility = View.VISIBLE
            }
            removeBtn?.visibility = View.VISIBLE

            setAsAdminBtn?.setOnClickListener {
                GroupAPI.instance.addAdming(GroupAdminModel(group.id!!, user.id!!))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    setAsAdminBtn.visibility = View.GONE
                                    user.isGroupAdmin = true
                                    notifyDataSetChanged()
                                },
                                {
                                    Log.e("HTTP ERROR", it.message)
                                    Toast.makeText(parent?.context, parent?.context?.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                })
            }

            removeBtn?.setOnClickListener {
                GroupAPI.instance.removeMember(GroupMemberRemoveModel(user?.id!!, group.id!!))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    group.members?.remove(user)
                                    notifyDataSetChanged()
                                },
                                {
                                    Log.e("HTTP ERROR", it.message)
                                    Toast.makeText(parent?.context, parent?.context?.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                })
            }
        } else {
            setAsAdminBtn?.visibility = View.INVISIBLE
            setAsAdminBtn?.setOnClickListener(null)
            removeBtn?.visibility = View.INVISIBLE
            removeBtn?.setOnClickListener(null)
        }


        return view!!
    }
}