package hr.zeroinfinity.adapters

import android.app.Activity
import android.content.Intent
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hr.zeroinfinity.R
import hr.zeroinfinity.activities.IncomeUpdateActivity
import hr.zeroinfinity.activities.StatisticActivity
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.IncomeModel

class DailyIncomesListAdapter(private val group: GroupModel) : ListAdapter<IncomeModel, DailyIncomesListAdapter.DailyIncomeViewHolder>(IncomeModel.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyIncomeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_element_income_daily, parent, false)
        return DailyIncomeViewHolder(view)
    }

    override fun onBindViewHolder(p0: DailyIncomeViewHolder, p1: Int) {
        p0.bind(getItem(p1), group)
    }

    inner class DailyIncomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(income: IncomeModel, group: GroupModel) {
            val amount = itemView.findViewById<TextView>(R.id.income_amount)
            val description = itemView.findViewById<TextView>(R.id.income_description)

            amount?.text = "${income?.amount} ${group.currency}"
            description?.text = income.description

            itemView.setOnLongClickListener {
                val context = (itemView.parent as ViewGroup).context
                val i = Intent(context, IncomeUpdateActivity::class.java)

                i.putExtra("model", income)

                (context as Activity).startActivityForResult(i, StatisticActivity.REQUEST_CODE)

                return@setOnLongClickListener true
            }

        }
    }
}