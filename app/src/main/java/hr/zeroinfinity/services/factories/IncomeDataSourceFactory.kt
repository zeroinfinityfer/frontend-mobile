package hr.zeroinfinity.services.factories

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import hr.zeroinfinity.models.IncomeModel
import hr.zeroinfinity.services.datasources.IncomeDataSource
import hr.zeroinfinity.services.retrofit.FinanceAPI
import java.util.*

class IncomeDataSourceFactory(val api: FinanceAPI, _groupId: UUID) : DataSource.Factory<Int, IncomeModel>() {
    private var groupId = _groupId

    var postLiveData: MutableLiveData<IncomeDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, IncomeModel> {
        val dataSource = IncomeDataSource(api, groupId)

        postLiveData.postValue(dataSource)

        return dataSource
    }

    fun setGroupId(_groupId: UUID) {
        groupId = _groupId
        create()
    }
}