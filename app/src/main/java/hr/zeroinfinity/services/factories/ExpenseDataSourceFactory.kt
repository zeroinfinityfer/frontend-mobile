package hr.zeroinfinity.services.factories

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.services.datasources.ExpenseDataSource
import hr.zeroinfinity.services.retrofit.FinanceAPI
import java.util.*

class ExpenseDataSourceFactory(val api: FinanceAPI, _groupId: UUID) : DataSource.Factory<Int, ExpenseModel>() {
    var postLiveData: MutableLiveData<ExpenseDataSource> = MutableLiveData()
    private var groupId: UUID = _groupId

    override fun create(): DataSource<Int, ExpenseModel> {
        val dataSource = ExpenseDataSource(api, groupId)

        postLiveData.postValue(dataSource)

        return dataSource
    }

    fun setGroupId(_groupId: UUID) {
        groupId = _groupId
        create()
    }
}