package hr.zeroinfinity.services.factories

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.services.datasources.GoalDataSource
import hr.zeroinfinity.services.retrofit.FinanceAPI
import java.util.*

class GoalDataSourceFactory(val api: FinanceAPI, _groupId: UUID) : DataSource.Factory<Int, GoalModel>() {
    var postLiveData: MutableLiveData<GoalDataSource> = MutableLiveData()
    private var groupId: UUID = _groupId

    override fun create(): DataSource<Int, GoalModel> {
        val dataSource = GoalDataSource(api, groupId)

        postLiveData.postValue(dataSource)

        return dataSource
    }

    fun setGroupId(_groupId: UUID) {
        groupId = _groupId
        create()
    }
}