package hr.zeroinfinity.services.retrofit

import com.google.gson.GsonBuilder
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.services.UserService
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class RetrofitContext {
    companion object {
        val instance: Retrofit by lazy(Retrofit.Builder()
                .baseUrl(BuildConfig.BACKEND_URL)
                .client(OkHttpClient.Builder()
                        .authenticator(Authenticator { _, response ->
                            if (UserService.refreshToken())
                                return@Authenticator response.request()
                                        .newBuilder()
                                        .header("Authorization", UserService.getAuthorizationHeader())
                                        .build()
                            else return@Authenticator null
                        })
                        .addInterceptor(Interceptor {
                            val request = it
                                    .request()
                                    .newBuilder()
                                    .addHeader("Authorization", UserService.getAuthorizationHeader())
                                    .addHeader("Accept-Language", Locale.getDefault().language)
                                    .build()

                            return@Interceptor it.proceed(request)
                        }).build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                        .setDateFormat("yyyy-MM-dd")
                        .create()))::build)
    }
}
