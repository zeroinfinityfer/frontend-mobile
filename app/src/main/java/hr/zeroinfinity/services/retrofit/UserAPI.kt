package hr.zeroinfinity.services.retrofit

import hr.zeroinfinity.models.DeviceSubscriptionModel
import hr.zeroinfinity.models.ResponseModel
import hr.zeroinfinity.models.UserModel
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface UserAPI {

    @GET("/user/email")
    fun getUserByEmail(@Query("email") email: String): Observable<UserModel>

    @POST("/user/register")
    fun registerUser(@Body userModel: UserModel): Observable<ResponseModel<UUID>>

    @GET("/user/forgotPassword")
    fun resetPassword(@Query("email") email: String): Observable<ResponseModel<Long>>

    @PUT("/user/device/subscribe")
    fun subscribeDevice(@Body subscriptionModel: DeviceSubscriptionModel): Observable<ResponseModel<Long>>

    @DELETE("/user/device/unsubscribe")
    fun unsubscribeDevice(@Query("deviceId") deviceId: String): Observable<ResponseModel<Long>>

    @GET("/user/identify")
    fun identify(): Observable<ResponseModel<Long>>

    @POST("/user/personalGroup")
    fun changePersonalGroupState(@Query("enabled") enabled: Boolean): Observable<ResponseModel<Long>>

    companion object {
        val instance: UserAPI by lazy {
            RetrofitContext.instance.create(UserAPI::class.java)
        }
    }
}
