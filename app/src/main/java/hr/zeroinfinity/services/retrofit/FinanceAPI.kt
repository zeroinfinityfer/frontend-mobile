package hr.zeroinfinity.services.retrofit


import hr.zeroinfinity.models.*
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface FinanceAPI {

    @GET("/finance/incomes")
    fun getIncomes(@Query("groupId") id: UUID, @Query("page") page: Int?, @Query("size") size: Int?): Observable<PageModel<IncomeModel>>

    @GET("/finance/expenses")
    fun getExpenses(@Query("groupId") id: UUID, @Query("page") page: Int?, @Query("size") size: Int?): Observable<PageModel<ExpenseModel>>

    @GET("/finance/goals")
    fun getGoals(@Query("groupId") id: UUID, @Query("page") page: Int?, @Query("size") size: Int?): Observable<PageModel<GoalModel>>

    @GET("/finance/categories")
    fun getCategories(@Query("groupId") id: UUID): Observable<MutableList<CategoryModel>>

    @POST("/category/register")
    fun registerCategory(@Body categoryModel: CategoryModel): Observable<ResponseModel<Long>>

    @PUT("/category/edit")
    fun editCategory(@Body categoryModel: CategoryModel): Observable<ResponseModel<Long>>

    @DELETE("/category/delete")
    fun deleteCategory(@Query("id") id: Long?): Observable<ResponseModel<Long>>

    @POST("/expense/register")
    fun registerExpense(@Body expenseModel: ExpenseModel): Observable<ResponseModel<Long>>

    @PUT("/expense/edit")
    fun editExpense(@Body expenseModel: ExpenseModel): Observable<ResponseModel<Long>>

    @DELETE("/expense/delete")
    fun deleteExpense(@Query("id") id: Long?): Observable<ResponseModel<Long>>

    @POST("/goal/register")
    fun registerGoal(@Body goalModel: GoalModel): Observable<ResponseModel<Long>>

    @PUT("/goal/edit")
    fun editGoal(@Body goalModel: GoalModel): Observable<ResponseModel<Long>>

    @DELETE("/goal/delete")
    fun deleteGoal(@Query("id") id: Long?): Observable<ResponseModel<Long>>

    @POST("/income/register")
    fun registerIncome(@Body incomeModel: IncomeModel): Observable<ResponseModel<Long>>

    @PUT("/income/edit")
    fun editIncome(@Body incomeModel: IncomeModel): Observable<ResponseModel<Long>>

    @DELETE("/income/delete")
    fun deleteIncome(@Query("id") id: Long?): Observable<ResponseModel<Long>>

    @GET("/expense/statistics")
    fun getExpensesStatistics(@Query("groupId") groupId: UUID, @Query("periodUnit") periodUnit: String, @Query("periodLength") periodLength: Int, @Query("toDate") toDate: String): Observable<Map<String, Double>>

    @GET("/income/statistics")
    fun getIncomesStatistics(@Query("groupId") groupId: UUID, @Query("periodUnit") periodUnit: String, @Query("periodLength") periodLength: Int, @Query("toDate") toDate: String): Observable<List<PairModel>>


    @GET("/savings/statistics")
    fun getSavingsStatistics(@Query("groupId") groupId: UUID, @Query("periodUnit") periodUnit: String, @Query("periodLength") periodLength: Int, @Query("toDate") toDate: String): Observable<SavingStatisticsModel>


    @POST("/savings/addTo")
    fun addToSaving(@Body savingEntryModel: SavingEntryModel): Observable<ResponseModel<Long>>


    @GET("/finance/statistics")
    fun getOverallStatistics(@Query("groupId") groupId: UUID, @Query("periodUnit") periodUnit: String, @Query("periodLength") periodLength: Int, @Query("toDate") toDate: String): Observable<FinanceStatisticsModel>


    @PUT("/goal/approve")
    fun approveGoal(@Query("goalId") goalId: Long, @Query("today") today: String): Observable<ResponseModel<Long>>


    @GET("/finance/statistics/daily")
    fun getDailyStatistics(@Query("groupId") groupId: UUID, @Query("date") date: String) : Observable<DailyStatisticsModel>

    @GET("/finance/statistics/calendar")
    fun getCalendarStatistics(@Query("groupId") groupId: UUID, @Query("periodUnit") periodUnit: String, @Query("periodLength") periodLength: Int, @Query("toDate") toDate: String): Observable<CalendarStatisticsModel>


    companion object {
        val instance: FinanceAPI by lazy {
            RetrofitContext.instance.create(FinanceAPI::class.java)
        }

        const val DEFAULT_PER_PAGE: Int = 10
    }
}
