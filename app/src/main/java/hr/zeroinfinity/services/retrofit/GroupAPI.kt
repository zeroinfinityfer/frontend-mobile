package hr.zeroinfinity.services.retrofit


import hr.zeroinfinity.models.*
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface GroupAPI {

    @get:GET("/group/")
    val groups: Observable<MutableList<GroupModel>>

    @get:GET("/group/invite")
    val invites: Observable<MutableList<InviteModel>>

    @POST("/group/register")
    fun registerGroup(@Body groupModel: GroupModel): Observable<ResponseModel<UUID>>

    @POST("/group/addMembers")
    fun inviteMembers(@Body groupInviteModel: GroupInviteModel): Observable<ResponseModel<Long>>

    @POST("/group/addMember")
    fun inviteMember(@Body memberInviteModel: MemberInviteModel): Observable<ResponseModel<Long>>

    @POST("/group/addAdmin")
    fun addAdming(@Body groupAdminModel: GroupAdminModel): Observable<ResponseModel<Long>>

    @POST("/group/removeMember")
    fun removeMember(@Body groupMemberRemoveModel: GroupMemberRemoveModel): Observable<ResponseModel<Long>>

    @PUT("/group/edit")
    fun editGroup(@Body groupModel: GroupModel): Observable<ResponseModel<UUID>>

    @POST("/group/removeAdmin")
    fun removeAdmin(@Body groupAdminRemoveModel: GroupAdminRemoveModel): Observable<ResponseModel<Long>>

    @PUT("/group/invite")
    fun handleInvite(@Query("inviteId") inviteId: Long?, @Query("response") response: Boolean?): Observable<ResponseModel<Long>>

    @DELETE("/group/delete")
    fun deleteGroup(@Query("groupId") groupId: UUID): Observable<ResponseModel<UUID>>

    companion object {
        val instance: GroupAPI by lazy {
            RetrofitContext.instance.create(GroupAPI::class.java)
        }
    }
}