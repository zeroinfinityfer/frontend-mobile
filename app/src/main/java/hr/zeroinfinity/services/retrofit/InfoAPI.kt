package hr.zeroinfinity.services.retrofit

import io.reactivex.Observable
import retrofit2.http.GET

interface InfoAPI{

    @GET("/info/version")
    fun getVersion(): Observable<String>

    companion object {
        val instance: InfoAPI by lazy {
            RetrofitContext.instance.create(InfoAPI::class.java)
        }
    }
}