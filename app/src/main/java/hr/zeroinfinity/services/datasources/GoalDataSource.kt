package hr.zeroinfinity.services.datasources

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import hr.zeroinfinity.common.NetworkState
import hr.zeroinfinity.common.RequestFailure
import hr.zeroinfinity.common.Retryable
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class GoalDataSource(private val financeAPI: FinanceAPI, var groupId: UUID) : PageKeyedDataSource<Int, GoalModel>() {

    private val requestFailureLiveData: MutableLiveData<RequestFailure> = MutableLiveData()
    private val networkStateLiveData: MutableLiveData<NetworkState> = MutableLiveData()


    fun getRequestFailureLiveData(): MutableLiveData<RequestFailure> {
        return requestFailureLiveData
    }

    fun getNetworkStateLiveData(): MutableLiveData<NetworkState> {
        return networkStateLiveData
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, GoalModel>) {
        val page = 0

        networkStateLiveData.postValue(NetworkState.LOADING)

        financeAPI
                .getGoals(groupId, page, params.requestedLoadSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            networkStateLiveData.postValue(NetworkState.LOADED)
                            callback.onResult(response.content!!, 0, response.totalElements!!, null, page + 1)
                        },
                        { error ->
                            networkStateLiveData.postValue(NetworkState.FAILED)

                            val retryable = object : Retryable {
                                override fun retry() {
                                    loadInitial(params, callback)
                                }
                            }

                            handleError(retryable, error)
                        }
                )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, GoalModel>) {
        val page = params.key

        financeAPI
                .getGoals(groupId, page, params.requestedLoadSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            networkStateLiveData.postValue(NetworkState.LOADED)
                            callback.onResult(response.content!!, page + 1)
                        },
                        { error ->
                            networkStateLiveData.postValue(NetworkState.FAILED)

                            val retryable = object : Retryable {
                                override fun retry() {
                                    loadAfter(params, callback);
                                }
                            }

                            handleError(retryable, error)
                        }
                )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, GoalModel>) {
        //valjda mozemo bez tog
    }

    private fun handleError(retryable: Retryable, t: Throwable) {
        requestFailureLiveData.postValue(RequestFailure(retryable, t.message!!))
    }
}
