package hr.zeroinfinity.services

import android.content.SharedPreferences
import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.common.Listener
import hr.zeroinfinity.common.Subject
import hr.zeroinfinity.common.SubjectAdapter
import hr.zeroinfinity.models.JwtTokenModel
import okhttp3.*
import java.io.IOException
import java.util.*

class UserService {
    companion object {
        private const val AUTHORIZATION: String = "Authorization"
        private const val BASIC_AUTH_HEADER: String = "Basic dXNlcjp1c2Vy"
        private const val PREFERENCES_JWT_TOKEN_KEY = "jwtToken"

        private const val LOGIN_PATH: String = "/oauth/token"
        private const val IDENTIFY_URL = "/user/identify"

        private val client = OkHttpClient()
        private var sharedPreferences: SharedPreferences? = null
        var jwtToken: JwtTokenModel? = null
        private val gson: Gson = Gson()

        fun initService(sharedPreferences: SharedPreferences, callback: Listener<Boolean?>) {
            this.sharedPreferences = sharedPreferences

            AsyncTask.execute {
                    val tokenAsString = sharedPreferences.getString(PREFERENCES_JWT_TOKEN_KEY, null)

                    if (tokenAsString != null) {
                        this.jwtToken = gson.fromJson(tokenAsString, JwtTokenModel::class.java)

                        try {
                            val authHeader = getAuthorizationHeader()
                            val resp = client.newCall(Request.Builder()
                                    .url(BuildConfig.BACKEND_URL + IDENTIFY_URL)
                                    .addHeader(AUTHORIZATION, authHeader)
                                    .build()).execute()

                            if (resp.code() == 200) {
                                callback.update(true)
                                return@execute
                            } else {
                                Log.d("AUTH_FAIL", resp.body()?.string())
                                callback.update(refreshToken())
                                return@execute
                            }
                        } catch (ex: IOException) {
                            callback.update(null)
                            return@execute
                        }
                    }

                    callback.update(false)
            }
        }

        fun refreshToken(): Boolean {
            if (this.jwtToken == null) return false
            val body = MultipartBody.Builder()
                    .addFormDataPart("refreshToken", this.jwtToken!!.refreshToken)
                    .addFormDataPart("grant_type", "password")
                    .setType(MultipartBody.FORM)
                    .build()

            val resp = client.newCall(
                    Request.Builder()
                            .url(BuildConfig.BACKEND_URL + LOGIN_PATH)
                            .post(body)
                            .header(AUTHORIZATION, getAuthorizationHeader())
                            .build()
            ).execute()

            if (resp.code() != 200) {
                this.jwtToken = null
                return false
            }

            this.jwtToken = gson.fromJson(resp.body()?.string(), JwtTokenModel::class.java)
            saveToken(resp.body()?.string())

            return true
        }

        private fun saveToken(token: String?) {
            this.sharedPreferences?.edit()?.putString(PREFERENCES_JWT_TOKEN_KEY, token)?.commit()
        }

        fun login(email: String, password: String): Subject<Boolean> {
            val body = MultipartBody.Builder()
                    .addFormDataPart("username", email.toLowerCase())
                    .addFormDataPart("password", password)
                    .addFormDataPart("grant_type", "password")
                    .setType(MultipartBody.FORM)
                    .build()

            val request = Request.Builder()
                    .url(BuildConfig.BACKEND_URL + LOGIN_PATH)
                    .post(body)
                    .headers(Headers.of(mutableMapOf(AUTHORIZATION to getAuthorizationHeader())))
                    .build()

            val ret = SubjectAdapter<Boolean>()
            client.newCall(request).enqueue(
                    object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            ret.notifyListeners(false)
                        }

                        @Throws(IOException::class)
                        override fun onResponse(call: Call, response: Response) {
                            if (response.code() == 200) {
                                val token = response.body()?.string()
                                Log.d("TOKEN", token)

                                this@Companion.jwtToken = gson.fromJson(token, JwtTokenModel::class.java)
                                saveToken(token)
                                ret.notifyListeners(true)
                            } else {
                                ret.notifyListeners(false)
                            }
                        }
                    })

            return ret
        }

        fun getAuthorizationHeader(): String {
            if (this.jwtToken == null) {
                return BASIC_AUTH_HEADER
            }

            return "Bearer ${this.jwtToken?.accessToken}"
        }

        fun getUserName(): String {
            if (this.jwtToken == null) {
                return "NONE"
            }

            return "${this.jwtToken?.userData?.get("name")} ${this.jwtToken?.userData?.get("surname")}"
        }

        fun getUserId(): UUID? {
            if (this.jwtToken == null) {
                return null
            }

            return UUID.fromString(this.jwtToken?.userData?.get("id"))
        }

        fun logout() {
            this.jwtToken = null
            sharedPreferences?.edit()?.remove(PREFERENCES_JWT_TOKEN_KEY)?.commit()
        }
    }
}