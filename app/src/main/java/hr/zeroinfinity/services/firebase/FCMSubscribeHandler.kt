package hr.zeroinfinity.services.firebase

import com.google.firebase.messaging.FirebaseMessaging
import hr.zeroinfinity.models.DeviceSubscriptionModel
import hr.zeroinfinity.models.ResponseModel
import hr.zeroinfinity.services.retrofit.UserAPI
import io.reactivex.Observable


object FCMSubscribeHandler {

    fun subscribeToTopic(topic: String) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
    }

    fun unsubscribeFromTopic(topic: String) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
    }

    fun subscribeDevice(tokenOld: String?, tokenNew: String): Observable<ResponseModel<Long>> {
        return UserAPI.instance.subscribeDevice(DeviceSubscriptionModel(tokenOld, tokenNew))
    }

    fun unsubscribeDevice(token: String): Observable<ResponseModel<Long>> {
        return UserAPI.instance.unsubscribeDevice(token)
    }
}
