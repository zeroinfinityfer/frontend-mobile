package hr.zeroinfinity.services.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import hr.zeroinfinity.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class MyFirebaseInstanceIDService : FirebaseMessagingService() {
    private var broadcastManager: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcastManager = LocalBroadcastManager.getInstance(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = CHANNEL_ID
            val descriptionText = CHANNEL_ID
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.d(TAG, "From: " + remoteMessage!!.from!!)
        Log.d(TAG, "Notification Message Body: " + remoteMessage.data)

        if(remoteMessage.notification != null){
            val intent = Intent(remoteMessage.notification?.clickAction!!).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            var mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(remoteMessage.notification?.title)
                    .setContentText(remoteMessage.notification?.body)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(remoteMessage.notification?.body))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)

            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                notify(RANDOM.nextInt(), mBuilder.build())
            }
        }

//        val intent = Intent()
//        intent.action = "NOW"
//        broadcastManager!!.sendBroadcast(intent)

        super.onMessageReceived(remoteMessage)
    }

    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.e("newToken", s)

        FCMSubscribeHandler.subscribeDevice(getSharedPreferences("_", Context.MODE_PRIVATE).getString("fb", "empty"), s!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("FCM", "Device subscribed")
                    getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("fb", s).apply()
                },
                        {
                            Log.e("FCM", "Device not subscribed")
                        })
    }

    companion object {
        private const val TAG = "FCM Service"
        private const val CHANNEL_ID = "cost.ly"
        private const val ENABLED = "enabled"
        private const val DISABLED = "disabled"
        private const val SP_NOTIF_KEY = "notif_stat";
        private val RANDOM = Random()

        fun getToken(context: Context): String {
            return context.getSharedPreferences("_", Context.MODE_PRIVATE).getString("fb", "empty")
        }

        fun notificationsEnabled(context: Context): Boolean {
            return ENABLED == context.getSharedPreferences("_", Context.MODE_PRIVATE).getString(SP_NOTIF_KEY, ENABLED)
        }

        fun changeNotificationsStatus(enabled: Boolean, context: Context) {
            context.getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString(SP_NOTIF_KEY, if (enabled) ENABLED else DISABLED).apply()
        }
    }
}
