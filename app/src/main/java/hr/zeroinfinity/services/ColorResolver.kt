package hr.zeroinfinity.services

import android.graphics.Color
import java.util.*

object ColorResolver {
    private val random: Random = Random()

    fun resolveValues(size: Int): List<Int> {
        val ret = mutableListOf<Int>()
        for (i in 0 until size) {
            ret.add(Color.argb(255,
                    random.nextInt(256)
                    , random.nextInt(256)
                    , random.nextInt(256)))

        }

        return ret
    }

    fun getRandomColor(): Int {
        return Color.argb(255,
                random.nextInt(256)
                , random.nextInt(256)
                , random.nextInt(256))
    }
}
