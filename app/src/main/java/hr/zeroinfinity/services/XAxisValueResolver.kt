package hr.zeroinfinity.services

import hr.zeroinfinity.models.PairModel

object XAxisValueResolver {
    fun resolveValues(models: List<PairModel>): List<String> {
        val ret = mutableListOf<String>()
        for (pair in models) {
            ret.add(pair.date!!)
        }

        return ret
    }
}
