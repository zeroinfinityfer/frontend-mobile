package hr.zeroinfinity.common

interface DialogWithResult<T> {

    fun setListener(listener: DialogResultListener<T>)
}
