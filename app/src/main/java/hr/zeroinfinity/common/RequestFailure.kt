package hr.zeroinfinity.common

class RequestFailure(val retryable: Retryable, val message: String)