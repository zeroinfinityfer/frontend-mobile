package hr.zeroinfinity.common

interface Subject<T> {

    fun addListener(listener: Listener<T>)

    fun removeListener(listener: Listener<T>)

    fun notifyListeners(data: T)
}
