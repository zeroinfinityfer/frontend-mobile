package hr.zeroinfinity.common

import android.util.Log
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

class AdInterstitialListenerImpl(private val ad: InterstitialAd) : AdListener() {

    override fun onAdFailedToLoad(p0: Int) {
        var error = ""
        when (p0) {
            AdRequest.ERROR_CODE_INTERNAL_ERROR -> error = "INTERNAL"
            AdRequest.ERROR_CODE_INVALID_REQUEST -> error = "INVALID REQUEST"
            AdRequest.ERROR_CODE_NETWORK_ERROR -> error = "NETWORK ERROR"
            AdRequest.ERROR_CODE_NO_FILL -> error = "NO FILL"
        }


        Log.e("AD_REQUEST", error)
        super.onAdFailedToLoad(p0)
    }

    override fun onAdLoaded() {
        super.onAdLoaded()

        ad.show()
    }
}