package hr.zeroinfinity.common

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListener
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.util.AttributeSet
import android.view.View
import com.getbase.floatingactionbutton.FloatingActionsMenu

class FloatingActionButtonBehavior : CoordinatorLayout.Behavior<FloatingActionsMenu> {

    constructor() : super() {
        animationOut = false
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        animationOut = false
    }

    override fun layoutDependsOn(parent: CoordinatorLayout, child: FloatingActionsMenu, dependency: View): Boolean {
        return dependency is Snackbar.SnackbarLayout
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: FloatingActionsMenu, dependency: View): Boolean {
        val translationY = Math.min(0f, dependency.translationY - dependency.height)
        child.translationY = translationY
        return true
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: FloatingActionsMenu, directTargetChild: View, target: View, nestedScrollAxes: Int): Boolean {
        return nestedScrollAxes == View.SCROLL_AXIS_VERTICAL || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes)
    }

    override fun onDependentViewRemoved(parent: CoordinatorLayout, child: FloatingActionsMenu, dependency: View) {
        super.onDependentViewRemoved(parent, child, dependency)
        child.translationY = 0f
    }


    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: FloatingActionsMenu, target: View,
                                dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed)

        if (dyConsumed > 0 && !animationOut && child.visibility == View.VISIBLE) {
            if (child.isExpanded) {
                child.collapse()
            }

            animateOut(child)
        } else if (dyConsumed < 0 && child.visibility != View.VISIBLE) {
            animateIn(child)
        }
    }

    companion object {

        private var animationOut = false
        private val INTERPOLATOR = FastOutSlowInInterpolator()


        fun animateIn(child: FloatingActionsMenu) {
            ViewCompat.animate(child).translationY(0f)
                    .setInterpolator(INTERPOLATOR)
                    .withLayer()
                    .setListener(object : ViewPropertyAnimatorListener {
                        override fun onAnimationStart(view: View) {
                            child.visibility = View.VISIBLE
                        }

                        override fun onAnimationEnd(view: View) {}

                        override fun onAnimationCancel(view: View) {
                        }
                    })
                    .start()
        }

        fun animateOut(child: FloatingActionsMenu) {
            ViewCompat.animate(child)
                    .translationY(500f)
                    .setInterpolator(INTERPOLATOR)
                    .withLayer()
                    .setListener(object : ViewPropertyAnimatorListener {
                        override fun onAnimationStart(view: View) {
                            animationOut = true
                        }

                        override fun onAnimationEnd(view: View) {
                            animationOut = false
                            view.visibility = View.INVISIBLE
                        }

                        override fun onAnimationCancel(view: View) {
                            animationOut = false
                        }
                    })
                    .start()
        }
    }
}