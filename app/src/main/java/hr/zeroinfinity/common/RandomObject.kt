package hr.zeroinfinity.common

import java.util.*

object RandomObject {
    private val random : Random = Random()

    fun getBoolean(percentage: Int) : Boolean{
        if(percentage < 0 || percentage > 100){
            throw IllegalArgumentException()
        }

        return random.nextInt(100) < percentage
    }
}