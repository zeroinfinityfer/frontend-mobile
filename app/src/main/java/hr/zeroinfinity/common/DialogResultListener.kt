package hr.zeroinfinity.common

interface DialogResultListener<T> {
    fun onResult(result: T)
}
