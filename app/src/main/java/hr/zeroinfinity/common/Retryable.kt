package hr.zeroinfinity.common

interface Retryable {
    fun retry()
}
