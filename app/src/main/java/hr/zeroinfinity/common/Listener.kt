package hr.zeroinfinity.common

interface Listener<T> {
    fun update(data: T)
}
