package hr.zeroinfinity.presenters

import hr.zeroinfinity.activities.MainActivity
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.UserService
import hr.zeroinfinity.services.firebase.FCMSubscribeHandler
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivityPresenter {
    private var view: MainActivity? = null
    private var disposable: Disposable? = null
    private val groupAPI: GroupAPI by lazy {
        GroupAPI.instance
    }
    var personalGroupEnabled: Boolean = false
    private var deviceSubscribed = false
    private var groupIndex: Int = -1
    var groups: MutableList<GroupModel> = mutableListOf()

    fun deviceSubscribed(){
        deviceSubscribed = true
    }

    fun deviceUnsubscribed() {
        deviceSubscribed = false
    }

    fun isDeviceSubscribed() : Boolean {
        return deviceSubscribed
    }

    fun bindView(_view: MainActivity) {
        view = _view

        if(getSelectedGroup() == null) {
            refresh()
        } else {
            _view.initView()
        }
    }

    fun setGroupIndex(index: Int) {
        if (index < groups.size) {
            groupIndex = index

            view?.updateView()
        }
    }

    fun getSelectedGroup(): GroupModel? {
        if(groupIndex < 0) return null
        return groups[groupIndex]
    }

    fun refresh() {
        disposable = groupAPI.groups.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            run {
                                personalGroupEnabled = false
                                unsubscribeFromAllGroups()
                                if (!result.isEmpty()) {
                                    groups = result
                                    for (group in result) {
                                        for (admin in group.admins!!) {
                                            if (admin.id == UserService.getUserId()) {
                                                group.activeUserIsAdmin = true
                                                break
                                            }
                                        }

                                        if(group.personal!!) personalGroupEnabled = true
                                        FCMSubscribeHandler.subscribeToTopic(group.id.toString())
                                    }

                                    if(groupIndex < 0 || groupIndex >= groups.size){
                                        groupIndex = 0
                                    }
                                } else {
                                    groups.clear()
                                    groupIndex = -1
                                }

                                view?.initView()
                            }
                        },
                        { error -> view?.onError(error) }
                )
    }

    private fun unsubscribeFromAllGroups(){
        for(group in groups){
            FCMSubscribeHandler.unsubscribeFromTopic(group.id.toString())
        }
    }

    fun reset() {
        groupIndex = -1
        groups = mutableListOf()
    }
}
