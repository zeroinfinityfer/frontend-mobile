package hr.zeroinfinity.activities

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.ExpensesListAdapter
import hr.zeroinfinity.common.NetworkState
import hr.zeroinfinity.common.NpaGridLayoutManager
import hr.zeroinfinity.common.RequestFailure
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.ColorResolver
import hr.zeroinfinity.services.datasources.ExpenseDataSource
import hr.zeroinfinity.services.executor.MainThreadExecutor
import hr.zeroinfinity.services.factories.ExpenseDataSourceFactory
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executor
import java.util.function.Function
import java.util.stream.Collectors


class ExpensesTabFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private val expenseListAdapter: ExpensesListAdapter? = ExpensesListAdapter(mutableMapOf(), null)
    private lateinit var recyclerView: RecyclerView
    private var expenseListViewHeader: ExpensesListAdapter.ExpenseListViewHeader? = null
    private var executor: Executor = MainThreadExecutor()
    private var dataSourceFactory: ExpenseDataSourceFactory? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private lateinit var progressBar: ProgressBar
    private lateinit var list: LiveData<PagedList<ExpenseModel>>

    companion object {
        const val registerExpenseRequestCode = 456
        const val updateExpenseRequestCode = 123

        val categoryValueMapper = Function<CategoryModel, CategoryModel> { t ->
            t.color = ColorResolver.getRandomColor()
            t
        }
        val categoryKeyMapper = Function<CategoryModel, Long> { t -> t.id!! }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        setGroup(arguments?.getParcelable("group")!!)
        return inflater.inflate(R.layout.fragment_expenses_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecycleView(view)
        expenseListViewHeader?.fetchData()
    }

    private fun initRecycleView(_view: View) {
        recyclerView = _view.findViewById(R.id.recycler_view_expense)!!
        swipeRefreshLayout = _view.findViewById(R.id.swipe_container_expenses)
        progressBar = _view.findViewById(R.id.progress_bar_list)

        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = NpaGridLayoutManager(_view.context, 1)
            recyclerView.setHasFixedSize(false)

            recyclerView.adapter = expenseListAdapter
            expenseListViewHeader = expenseListAdapter?.ExpenseListViewHeader(_view)!!
            expenseListViewHeader?.bindTo()

            swipeRefreshLayout?.setOnRefreshListener(this)
        }
    }

    private fun setGroup(selectedGroup: GroupModel) {
        val categoriesMap = selectedGroup
                .categories!!
                .stream()
                .collect(Collectors.toMap(categoryKeyMapper, categoryValueMapper))

        expenseListAdapter?.categories = categoriesMap
        expenseListAdapter?.group = selectedGroup

        initDataSource(selectedGroup.id)
    }

    private fun initDataSource(groupId: UUID?) {
        dataSourceFactory = ExpenseDataSourceFactory(FinanceAPI.instance, groupId!!)
        dataSourceFactory?.postLiveData?.observeForever(Observer<ExpenseDataSource> {
            it?.getRequestFailureLiveData()?.observeForever(Observer<RequestFailure> { requestFailure ->
                if (requestFailure == null) return@Observer

                Snackbar.make(this@ExpensesTabFragment.view!!,
                        getString(R.string.server_unavailable_label),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry_button_label)) {
                            requestFailure.retryable.retry()
                        }.show()
            })

            it?.getNetworkStateLiveData()?.observeForever { it1 ->
                progressBar.visibility = if(it1 == NetworkState.LOADING) View.VISIBLE else View.INVISIBLE
            }
        })

        val config = PagedList.Config.Builder()
                .setPageSize(FinanceAPI.DEFAULT_PER_PAGE)
                .setInitialLoadSizeHint(FinanceAPI.DEFAULT_PER_PAGE * 2)
                .setEnablePlaceholders(true)
                .build()

        list = LivePagedListBuilder(dataSourceFactory!!, config)
                .setFetchExecutor(executor)
                .build()

        expenseListAdapter?.submitList(list.value)
        list.observeForever {
            expenseListAdapter?.submitList(it)
        }
    }

    override fun onRefresh() {
        swipeRefreshLayout?.isRefreshing = false
        FinanceAPI.instance.getCategories(expenseListAdapter?.group?.id!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { r ->
                            val categoriesMap = r?.stream()?.collect(Collectors.toMap(categoryKeyMapper, categoryValueMapper))

                            expenseListAdapter.categories = categoriesMap!!
                            expenseListAdapter.group?.categories = r
                            expenseListViewHeader?.fetchData()

                            dataSourceFactory?.postLiveData?.value?.invalidate()
                        },
                        { e ->
                            swipeRefreshLayout?.isRefreshing = false
                            Log.d("HTTP ERROR", e.message)
                            Toast.makeText(
                                    this@ExpensesTabFragment.context
                                    , getString(R.string.server_unavailable_label)
                                    , Toast.LENGTH_SHORT).show()
                        }
                )
    }

    override fun onPause() {
        swipeRefreshLayout?.isRefreshing = false
        super.onPause()
    }
}


