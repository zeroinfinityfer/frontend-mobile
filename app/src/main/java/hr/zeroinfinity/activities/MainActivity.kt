package hr.zeroinfinity.activities


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SwitchCompat
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import com.getbase.floatingactionbutton.FloatingActionButton
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.firebase.iid.FirebaseInstanceId
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.FragmentAdapter
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.FloatingActionButtonBehavior
import hr.zeroinfinity.models.GroupMemberRemoveModel
import hr.zeroinfinity.models.SavingEntryModel
import hr.zeroinfinity.presenters.MainActivityPresenter
import hr.zeroinfinity.services.UserService
import hr.zeroinfinity.services.firebase.FCMSubscribeHandler
import hr.zeroinfinity.services.retrofit.GroupAPI
import hr.zeroinfinity.services.retrofit.UserAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var addExpenseButton: FloatingActionButton
    private lateinit var addIncomeButton: FloatingActionButton
    private lateinit var addGoalButton: FloatingActionButton
    private lateinit var addToSavingButton: FloatingActionButton
    private lateinit var fabActionMenu: FloatingActionsMenu

    private lateinit var switch: SwitchCompat
    private var fragmentAdapter: FragmentAdapter? = null
    private var mViewPager: ViewPager? = null
    private var tabLayout: TabLayout? = null
    private var drawerLayout: DrawerLayout? = null
    private var mToggle: ActionBarDrawerToggle? = null
    private var allTabFragment: AllTabFragment? = null
    private var expensesTabFragment: ExpensesTabFragment? = null
    private var incomesTabFragment: IncomesTabFragment? = null
    private var goalsTabFragment: GoalsTabFragment? = null
    private lateinit var toolbarMenu: Menu
    private lateinit var navigationView: NavigationView
    private lateinit var TAB_NAMES: Array<String>

    private val addExpenseListener: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            if (presenter.getSelectedGroup() == null) return

            val i = Intent(this@MainActivity, ExpenseRegisterActivity::class.java)
            i.putExtra("groupId", presenter.getSelectedGroup()?.id)
            i.putParcelableArrayListExtra("categories", ArrayList(presenter.getSelectedGroup()?.categories!!))
            i.putExtra("isFromMain", true)

            startActivityForResult(i, ExpensesTabFragment.registerExpenseRequestCode)
            fabActionMenu.collapse()
        }
    }

    private val addIncomeListener: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            if (presenter.getSelectedGroup() == null) return

            val i = Intent(this@MainActivity, IncomeRegisterActivity::class.java)
            i.putExtra("groupId", presenter.getSelectedGroup()?.id)
            i.putExtra("isFromMain", true)

            startActivityForResult(i, IncomesTabFragment.registerIncomeRequestCode)
            fabActionMenu.collapse()
        }
    }

    private val addGoalListener: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            if (presenter.getSelectedGroup() == null) return

            val i = Intent(this@MainActivity, GoalRegisterActivity::class.java)
            i.putExtra("groupId", presenter.getSelectedGroup()?.id)
            i.putExtra("isFromMain", true)

            startActivityForResult(i, GoalsTabFragment.registerGoalRequestCode)
            fabActionMenu.collapse()
        }
    }

    private val adapter = object : FragmentStatePagerAdapter(supportFragmentManager) {
        override fun getItem(position: Int): Fragment {
            val selectedGroup = presenter.getSelectedGroup()

            val bundle = Bundle()
            bundle.putParcelable("group", selectedGroup)
            return when (position) {
                0 -> {
                    allTabFragment = AllTabFragment()
                    allTabFragment?.arguments = bundle

                    allTabFragment!!
                }

                1 -> {
                    expensesTabFragment = ExpensesTabFragment()
                    expensesTabFragment?.arguments = bundle

                    expensesTabFragment!!
                }

                2 -> {
                    incomesTabFragment = IncomesTabFragment()
                    incomesTabFragment?.arguments = bundle

                    incomesTabFragment!!
                }

                3 -> {
                    goalsTabFragment = GoalsTabFragment()
                    goalsTabFragment?.arguments = bundle

                    goalsTabFragment!!
                }

                else -> AllTabFragment()
            }
        }

        override fun getCount(): Int {
            return TAB_NAMES.size
        }

        override fun getItemPosition(fragItem: Any): Int {
            var position = 0
            when (fragItem) {
                is AllTabFragment -> position = 0
                is ExpensesTabFragment -> position = 1
                is IncomesTabFragment -> position = 2
                is GoalsTabFragment -> position = 3
            }

            return if (position >= 0) position else POSITION_NONE
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> TAB_NAMES[0]
                1 -> TAB_NAMES[1]
                2 -> TAB_NAMES[2]
                3 -> TAB_NAMES[3]

                else -> TAB_NAMES[0]
            }
        }
    }

    private val onCheckedChangeListener: CompoundButton.OnCheckedChangeListener =
            CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                buttonView?.isEnabled = false
                UserAPI.instance.changePersonalGroupState(isChecked)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    buttonView?.isEnabled = true
                                    presenter.refresh()
                                },
                                {
                                    Log.e("HTTP ERROR", it.message)
                                    buttonView?.isEnabled = true
                                    buttonView?.isChecked = !isChecked
                                })
            }

    private val messageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //presenter.refresh()
        }
    }

    private val tabSelectedListener: TabLayout.OnTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(p0: TabLayout.Tab?) {
        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {
        }

        override fun onTabSelected(p0: TabLayout.Tab?) {
            if (fabActionMenu.visibility != View.VISIBLE) FloatingActionButtonBehavior.animateIn(fabActionMenu)

            if (mViewPager?.currentItem == 0) {

            }
        }
    }

    private val addToSavingListener: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            if (presenter.getSelectedGroup() == null) return

            val dialog = SavingDialogFragment()
            dialog.setListener(object : DialogResultListener<SavingEntryModel> {
                override fun onResult(result: SavingEntryModel) {
                    presenter.getSelectedGroup()?.savingsAmount = presenter.getSelectedGroup()?.savingsAmount?.add(result.amountAdded)
                }
            })

            val bundle = Bundle()
            bundle.putString("groupId", presenter.getSelectedGroup()?.id.toString())
            dialog.arguments = bundle

            dialog.show(supportFragmentManager, "add_to_saving_dialog")
            fabActionMenu.collapse()
        }
    }

    private val fabActionMenuListener: FloatingActionsMenu.OnFloatingActionsMenuUpdateListener = object : FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
        override fun onMenuCollapsed() {
        }

        override fun onMenuExpanded() {
            val currentTabIndex = mViewPager?.currentItem
            if (currentTabIndex != 0) {
                fabActionMenu.collapseImmediately()

                when (currentTabIndex) {
                    1 -> addExpenseListener.onClick(null)
                    2 -> addIncomeListener.onClick(null)
                    3 -> addGoalListener.onClick(null)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addExpenseButton = findViewById(R.id.addExpense)
        addIncomeButton = findViewById(R.id.addIncome)
        addGoalButton = findViewById(R.id.addGoal)
        addToSavingButton = findViewById(R.id.addToSaving)
        fabActionMenu = findViewById(R.id.fab)

        TAB_NAMES = arrayOf(
                resources.getString(R.string.all),
                resources.getString(R.string.expenses),
                resources.getString(R.string.incomes),
                resources.getString(R.string.savings))

        fragmentAdapter = FragmentAdapter(supportFragmentManager)
        mViewPager = findViewById(R.id.pager)

        tabLayout = findViewById(R.id.tabs)
        tabLayout?.addOnTabSelectedListener(tabSelectedListener)
        navigationView = findViewById(R.id.navigationView)
        tabLayout?.setupWithViewPager(mViewPager, true)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close)
        drawerLayout?.addDrawerListener(mToggle!!)
        mToggle?.syncState()

        supportActionBar?.title = ""

        addExpenseButton.setOnClickListener(addExpenseListener)
        addIncomeButton.setOnClickListener(addIncomeListener)
        addGoalButton.setOnClickListener(addGoalListener)
        addToSavingButton.setOnClickListener(addToSavingListener)
        fabActionMenu.setOnFloatingActionsMenuUpdateListener(fabActionMenuListener)

        navigationView.setNavigationItemSelectedListener {
            drawerLayout?.closeDrawers()

            presenter.setGroupIndex(it.itemId)
            val group = presenter.getSelectedGroup()
            supportActionBar?.title = if (group?.personal!!) getString(R.string.your_statistics_label) else group.name

            return@setNavigationItemSelectedListener true
        }

        navigationView.menu.findItem(R.id.add_new_group_item).setOnMenuItemClickListener {
            val i = Intent(this, GroupRegisterActivity::class.java)
            startActivityForResult(i, registerGroupRequestCode)
            return@setOnMenuItemClickListener true
        }

        navigationView.menu.findItem(R.id.go_to_invites_list).setOnMenuItemClickListener {
            val i = Intent(this, InvitesManagementActivity::class.java)
            startActivityForResult(i, registerGroupRequestCode)
            return@setOnMenuItemClickListener true
        }

        navigationView.menu.findItem(R.id.logout_button).setOnMenuItemClickListener {
            FCMSubscribeHandler.unsubscribeDevice(getSharedPreferences("_", Context.MODE_PRIVATE).getString("fb", "empty")!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        getSharedPreferences("_", Context.MODE_PRIVATE).edit()?.remove("fb")?.apply()
                        presenter.deviceUnsubscribed()
                        UserService.logout()
                        presenter.reset()
                        finish()
                    },
                            {
                                Toast.makeText(this@MainActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                            })

            return@setOnMenuItemClickListener true
        }

        navigationView.getHeaderView(0).findViewById<TextView>(R.id.name_tag).text = UserService.getUserName()
        switch = navigationView.menu.findItem(R.id.personal_stat_enabled).actionView.findViewById(R.id.drawer_switch)

        if (!presenter.isDeviceSubscribed()) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                Log.d("FCM", "New token " + it.token)
                FCMSubscribeHandler.subscribeDevice(null, it.token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .subscribe({ _ ->
                            run {
                                getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("fb", it.token).apply()
                                presenter.deviceSubscribed()
                                Log.d("FCM", "Device subscribed")
                            }
                        },
                                {
                                    Log.e("FCM", "Device not subscribed")
                                })
            }
        }

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (registerGroupRequestCode == requestCode) presenter.refresh()
            else if (IncomesTabFragment.registerIncomeRequestCode == requestCode || IncomesTabFragment.updateIncomeRequestCode == requestCode) {
                incomesTabFragment?.onRefresh()
                allTabFragment?.onRefresh()
            } else if (ExpensesTabFragment.registerExpenseRequestCode == requestCode || IncomesTabFragment.updateIncomeRequestCode == requestCode) {
                expensesTabFragment?.onRefresh()
                allTabFragment?.onRefresh()
            } else if (GoalsTabFragment.registerGoalRequestCode == requestCode || GoalsTabFragment.updateGoalRequestCode == requestCode) goalsTabFragment?.onRefresh()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        toolbarMenu = menu

        presenter.bindView(this)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val selectedGroup = presenter.getSelectedGroup()
        if (mToggle?.onOptionsItemSelected(item)!!) {
            return true
        } else if (item?.itemId == R.id.statistic) {
            val i = Intent(this, StatisticActivity::class.java)
            i.putExtra("group", selectedGroup)
            startActivity(i)
        } else if (item?.itemId == R.id.group_admin) {
            val i = Intent(this, GroupManagementActivity::class.java)
            i.putExtra("group", selectedGroup)

            startActivityForResult(i, registerGroupRequestCode)
        } else if (item?.itemId == R.id.group_leave) {
            if (selectedGroup?.activeUserIsAdmin!! && selectedGroup.admins?.size == 1 && selectedGroup.members?.size != 1) {
                Toast.makeText(this, getString(R.string.add_new_admin_warning), Toast.LENGTH_SHORT).show()
            } else {
                AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.leaving_group_dialog_label))
                        .setMessage(getString(R.string.are_you_sure_label))
                        .setPositiveButton(getString(R.string.yes)) { _, _ ->
                            run {
                                GroupAPI.instance.removeMember(GroupMemberRemoveModel(UserService.getUserId()!!, selectedGroup.id!!))
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            presenter.refresh()
                                        },
                                                {
                                                    Log.e("HTTP ERROR", it.message)
                                                    Toast.makeText(this, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                                })
                            }
                        }.setNegativeButton(getString(R.string.no), null)
                        .show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun initView() {
        switch.setOnCheckedChangeListener(null)
        switch.isChecked = presenter.personalGroupEnabled
        switch.setOnCheckedChangeListener(onCheckedChangeListener)

        refreshDrawer()
        refreshToolbarMenu()
        setupViewPager(mViewPager!!)
    }

    private fun refreshToolbarMenu() {
        val selectedGroup = presenter.getSelectedGroup()
        val leaveGroupItem = toolbarMenu.findItem(R.id.group_leave)
        val aboutGroupItem = toolbarMenu.findItem(R.id.group_admin)
        val statisticItem = toolbarMenu.findItem(R.id.statistic)

        if (selectedGroup == null) {
            leaveGroupItem.isVisible = false
            aboutGroupItem.isVisible = false
            statisticItem.isVisible = false
            supportActionBar?.title = ""
        } else {
            supportActionBar?.title = if (selectedGroup.personal!!) getString(R.string.your_statistics_label) else selectedGroup.name!!
            leaveGroupItem.isVisible = !selectedGroup.personal!!
            aboutGroupItem.isVisible = !selectedGroup.personal!!
            statisticItem.setShowAsAction(if (!selectedGroup.personal!!) MenuItem.SHOW_AS_ACTION_NEVER else MenuItem.SHOW_AS_ACTION_IF_ROOM)
        }
    }

    fun updateView() {
        switch.isChecked = presenter.personalGroupEnabled
        if (fabActionMenu.visibility != View.VISIBLE) FloatingActionButtonBehavior.animateIn(fabActionMenu)
        setupViewPager(mViewPager!!)
        refreshToolbarMenu()
    }

    fun onError(error: Any) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show()
    }

    private fun setupViewPager(viewPager: ViewPager) {
        if (presenter.getSelectedGroup() == null) {
            viewPager.adapter = null
            return
        }

        viewPager.adapter = adapter
    }

    private fun refreshDrawer() {
        val subMenu = navigationView.menu.findItem(R.id.m_area).subMenu

        if (subMenu.hasVisibleItems()) {
            subMenu.clear()
        }

        for (i in 0 until presenter.groups.size) {
            val group = presenter.groups[i]
            subMenu.add(R.id.drawerGroup, i, Menu.NONE, if (group.personal!!) getString(R.string.your_statistics_label) else group.name)
        }
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.exit))
                .setMessage(getString(R.string.exit_question))
                .setPositiveButton(getString(R.string.yes)) { _, _ -> moveTaskToBack(true) }
                .setNegativeButton(getString(R.string.no), null)
                .show()

    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, IntentFilter("NOW"))
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver)
    }

    companion object {
        const val registerGroupRequestCode = 537
        private val presenter: MainActivityPresenter = MainActivityPresenter()
    }
}



