package hr.zeroinfinity.activities

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.models.UserModel
import hr.zeroinfinity.services.retrofit.UserAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private val userModel: UserModel = UserModel()
    private val userAPI: UserAPI by lazy {
        UserAPI.instance
    }

    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@RegisterActivity, R.anim.shake)
    }

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerButton.setOnClickListener(this)

        cancelButton.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        supportActionBar?.title = ""
        val currencyAdapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                resources.getStringArray(R.array.currency_codes))
        val pos = currencyAdapter.getPosition("EUR")
        currency_spinner_register.adapter = currencyAdapter
        currency_spinner_register.setSelection(pos)
    }

    private fun validRegisterMail(userModel: UserModel): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(userModel.email).matches()
    }

    private fun validRegisterPassword(userModel: UserModel): Boolean {
        if (userModel.password!!.length < 8 || userModel.password!!.length > 50) {
            return false
        }

        return true
    }

    private fun validAllFieldsFilled(userModel: UserModel): Boolean {
        var valid = true
        val errorMessage = getString(R.string.required_field_error)

        if (userModel.name.isNullOrEmpty()) {
            nameInput.error = errorMessage
            nameInput.startAnimation(shake)
            valid = false
        }

        if (userModel.surname.isNullOrEmpty()) {
            surnameInput.error = errorMessage
            surnameInput.startAnimation(shake)
            valid = false
        }

        if (userModel.email.isNullOrEmpty()) {
            emailInput.error = errorMessage
            emailInput.startAnimation(shake)
            valid = false
        }

        if (userModel.password.isNullOrEmpty()) {
            passwordInput.error = errorMessage
            passwordInput.startAnimation(shake)
            valid = false
        }

        if (userModel.confirmPassword.isNullOrEmpty()) {
            confirmPasswordInput.error = errorMessage
            confirmPasswordInput.startAnimation(shake)
            valid = false
        }

        return valid
    }

    private fun validRegisterMatchingPasswords(password: String?, confirmedPassword: String?): Boolean {
        return password == confirmedPassword
    }

    override fun onClick(v: View?) {
        userModel.name = nameInput.text.toString().trim()
        userModel.surname = surnameInput.text.toString().trim()
        userModel.email = emailInput.text.toString().trim()
        userModel.password = passwordInput.text.toString()
        userModel.personalGroupEnabled = personalGroupEnabledCheckBox.isChecked
        userModel.confirmPassword = confirmPasswordInput.text.toString()
        userModel.currency = currency_spinner_register.selectedItem as String


        if (!validAllFieldsFilled(userModel)) {
        } else if (!validRegisterMail(userModel)) {
            emailInput.text.clear()
            emailInput.error = getString(R.string.email_format_error)
            emailInput.startAnimation(shake)
        } else if (!validRegisterMatchingPasswords(userModel.password, userModel.confirmPassword)) {
            confirmPasswordInput.text.clear()
            confirmPasswordInput.error = getString(R.string.confirm_password_error)
            confirmPasswordInput.startAnimation(shake)
        } else if (!validRegisterPassword(userModel)) {
            passwordInput.text.clear()
            confirmPasswordInput.text.clear()
            passwordInput.error = getString(R.string.password_too_short_error)
            passwordInput.startAnimation(shake)
        } else {
            disposable = userAPI.registerUser(userModel)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                setResult(Activity.RESULT_OK)
                                finish()
                            },
                            { error ->
                                if (error.message?.trim().equals("HTTP 422", true)) {
                                    emailInput.text.clear()
                                    emailInput.error = getString(R.string.email_in_use_error)
                                    emailInput.startAnimation(shake)
                                } else {
                                    Log.d("HTTP ERROR", error.message)
                                    Toast.makeText(
                                            this@RegisterActivity
                                            , getString(R.string.server_unavailable_label)
                                            , Toast.LENGTH_SHORT).show()
                                }
                            }
                    )
        }
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}



