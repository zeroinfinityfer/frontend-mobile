package hr.zeroinfinity.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.EventDay
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.DailyExpensesListAdapter
import hr.zeroinfinity.adapters.DailyGoalsListAdapter
import hr.zeroinfinity.adapters.DailyIncomesListAdapter
import hr.zeroinfinity.common.AdInterstitialListenerImpl
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.NpaGridLayoutManager
import hr.zeroinfinity.models.FinanceStatsEventDay
import hr.zeroinfinity.models.GoalMarkersEventDay
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.PairModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.function.Consumer

class StatisticActivity : AppCompatActivity() {

    private lateinit var calendarView: CalendarView
    private lateinit var progressBar1: ProgressBar
    private lateinit var progressBar2: ProgressBar
    private lateinit var group: GroupModel
    private lateinit var expensesListView: RecyclerView
    private lateinit var incomesListView: RecyclerView
    private lateinit var goalsListView: RecyclerView
    private lateinit var expensesListFrame: LinearLayout
    private lateinit var incomesListFrame: LinearLayout
    private lateinit var goalsListFrame: LinearLayout
    private lateinit var dailyExpensesListAdapter: DailyExpensesListAdapter
    private lateinit var dailyIncomesListAdapter: DailyIncomesListAdapter
    private lateinit var dailyGoalsListAdapter: DailyGoalsListAdapter
    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)

        val bundle = intent.extras
        if (bundle != null) {
            group = bundle.getParcelable("group") as GroupModel
        } else {
            finish()
        }

        calendarView = findViewById(R.id.calendarView)
        progressBar1 = findViewById(R.id.progressBar_cyclic_1)
        progressBar2 = findViewById(R.id.progressBar_cyclic_2)
        expensesListView = findViewById(R.id.expenses_list_view)
        incomesListView = findViewById(R.id.incomes_list_view)
        goalsListView = findViewById(R.id.goals_list_view)
        expensesListFrame = findViewById(R.id.expenses_list_frame_layout)
        incomesListFrame = findViewById(R.id.incomes_list_frame_layout)
        goalsListFrame = findViewById(R.id.goals_list_frame_layout)

        dailyExpensesListAdapter = DailyExpensesListAdapter(group)
        dailyIncomesListAdapter = DailyIncomesListAdapter(group)
        dailyGoalsListAdapter = DailyGoalsListAdapter(group)

        expensesListView.layoutManager = NpaGridLayoutManager(this, 1)
        expensesListView.setHasFixedSize(false)

        incomesListView.layoutManager = NpaGridLayoutManager(this, 1)
        incomesListView.setHasFixedSize(false)

        goalsListView.layoutManager = NpaGridLayoutManager(this, 1)
        goalsListView.setHasFixedSize(false)

        expensesListView.adapter = dailyExpensesListAdapter
        incomesListView.adapter = dailyIncomesListAdapter
        goalsListView.adapter = dailyGoalsListAdapter

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""

        initCalendar()

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        val mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.adListener = AdInterstitialListenerImpl(mInterstitialAd)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            fetchDailyStats(calendarView.selectedDate?.time)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun initCalendar() {
        calendarView.setDate(Date())

        calendarView.setOnForwardPageChangeListener {
            hideDailyStats()
            fetchCalendarData()
        }
        calendarView.setOnPreviousPageChangeListener {
            hideDailyStats()
            fetchCalendarData()
        }

        calendarView.setOnDayClickListener {
            if (it is FinanceStatsEventDay || it is GoalMarkersEventDay) {
                fetchDailyStats(it.calendar.time)
            } else {
                hideDailyStats()
            }
        }


        fetchCalendarData()
        fetchDailyStats(calendarView.selectedDate?.time)
    }

    private fun hideDailyStats() {
        expensesListFrame.visibility = View.GONE
        incomesListFrame.visibility = View.GONE
        goalsListFrame.visibility = View.GONE
    }

    private fun fetchDailyStats(date: Date?) {
        progressBar2.visibility = View.VISIBLE
        FinanceAPI.instance.getDailyStatistics(group.id!!, DATE_FORMAT.format(date!!))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            if (it.expenses === null) {
                                expensesListFrame.visibility = View.GONE
                            } else {
                                (expensesListView.adapter as DailyExpensesListAdapter).submitList(it.expenses)
                                expensesListFrame.visibility = View.VISIBLE
                            }

                            if (it.incomes === null) {
                                incomesListFrame.visibility = View.GONE
                            } else {
                                (incomesListView.adapter as DailyIncomesListAdapter).submitList(it.incomes)
                                incomesListFrame.visibility = View.VISIBLE
                            }

                            if (it.goals === null) {
                                goalsListFrame.visibility = View.GONE
                            } else {
                                (goalsListView.adapter as DailyGoalsListAdapter).submitList(it.goals)
                                goalsListFrame.visibility = View.VISIBLE
                            }

                            progressBar2.visibility = View.INVISIBLE
                        },
                        { error ->
                            Log.d("HTTP ERROR", error.message)
                            Toast.makeText(
                                    this@StatisticActivity
                                    , getString(R.string.server_unavailable_label)
                                    , Toast.LENGTH_SHORT).show()
                            progressBar2.visibility = View.INVISIBLE
                        })
    }

    private fun fetchCalendarData() {
        progressBar1.visibility = View.VISIBLE
        val firstPageCal = calendarView.currentPageDate

        val periodLength = firstPageCal.getActualMaximum(Calendar.DAY_OF_MONTH)
        firstPageCal.add(Calendar.MONTH, 1)
        firstPageCal.add(Calendar.DATE, -1)
        val toDate = DATE_FORMAT.format(firstPageCal.time)

        disposable = FinanceAPI.instance.getCalendarStatistics(group.id!!, "d", periodLength, toDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            val events = mutableListOf<EventDay>()

                            result.goals?.forEach(Consumer {
                                events.add(GoalMarkersEventDay(it))
                            })

                            result.dailyBalance?.forEach(Consumer<PairModel> {
                                if (it.value != 0.0) events.add(FinanceStatsEventDay(this@StatisticActivity, it))
                            })

                            calendarView.setEvents(events)
                            progressBar1.visibility = View.INVISIBLE
                        },
                        { error ->
                            Log.d("HTTP ERROR", error.message)
                            Toast.makeText(
                                    this@StatisticActivity
                                    , getString(R.string.server_unavailable_label)
                                    , Toast.LENGTH_SHORT).show()
                            progressBar1.visibility = View.INVISIBLE
                        })

    }

    companion object {
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        val REQUEST_CODE = 13213
    }
}
