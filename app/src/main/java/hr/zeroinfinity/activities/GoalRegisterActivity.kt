package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_goal_register.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

class GoalRegisterActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private var goalDefinitionValue: String? = null
    private var goalDescriptionValue: String? = null
    private var goalTargetValue: Double? = null
    private var goalDateCreatedValue: Date = Date()
    private lateinit var groupId: UUID
    private var isFromMain: Boolean = false
    private var disposable: Disposable? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }
    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@GoalRegisterActivity, R.anim.shake)
    }
    private lateinit var mInterstitialAd : InterstitialAd

    private var datePickerDialog: DatePickerDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goal_register)

        goalDateCreated.text = SimpleDateFormat(getString(R.string.date_format_formatter)).format(this.goalDateCreatedValue)

        val cal = Calendar.getInstance()
        cal.time = goalDateCreatedValue
        val currentYear = cal.get(Calendar.YEAR)
        val currentMonth = cal.get(Calendar.MONTH)
        val currentDay = cal.get(Calendar.DAY_OF_MONTH)

        datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
        goalDateCreated.setOnClickListener {
            goalDateCreated.error = null
            datePickerDialog?.show()
        }

        val bundle = intent.extras
        if (bundle != null) {
            groupId = bundle.get("groupId") as UUID
            isFromMain = bundle.get("isFromMain") as Boolean
        }

        addGoalButton.setOnClickListener(this)
        cancelGoalButton.setOnClickListener {
            val i = Intent(this, if (isFromMain) AllTabFragment::class.java else GoalsTabFragment::class.java)
            setResult(Activity.RESULT_CANCELED, i)
            finish()
        }

        goal_register_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        goalDateCreated.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.goalDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }

    override fun onClick(v: View?) {
        var f = true
        if (goalDefinition?.text.isNullOrEmpty()) {
            goalDefinition?.error = getString(R.string.required_field_error)
            goalDefinition?.animation = shake
            f = false
        } else
            this.goalDefinitionValue = goalDefinition?.text.toString()

        when {
            goalTarget?.text.isNullOrEmpty() -> {
                goalTarget?.error = getString(R.string.required_field_error)
                goalTarget?.animation = shake
                f = false
            }
            goalTarget?.text.toString().toDouble() <= 0 -> {
                goalTarget?.error = getString(R.string.required_positive_number_error)
                goalTarget?.animation = shake
                f = false
            }
            else -> this.goalTargetValue = goalTarget?.text.toString().toDouble()
        }

        this.goalDescriptionValue = goalDescription?.text.toString()

        if (f) {
            val model = GoalModel()
            model.targetAmount = BigDecimal.valueOf(goalTargetValue!!)
            model.targetDate = goalDateCreatedValue
            model.description = goalDescriptionValue
            model.name = goalDefinitionValue
            model.groupId = groupId

            disposable = financeAPI.registerGoal(model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { r ->
                                run {
                                    val i = Intent(this, if (isFromMain) AllTabFragment::class.java else GoalsTabFragment::class.java)

                                    setResult(Activity.RESULT_OK, i)
                                    if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                        mInterstitialAd.adListener = object : AdListener() {
                                            override fun onAdClosed() {
                                                super.onAdClosed()
                                                finish()
                                            }
                                        }

                                        mInterstitialAd.show()
                                    } else {
                                        finish()
                                    }
                                }
                            },
                            { e ->
                                Log.d("HTTP ERROR", e.message)
                                Toast.makeText(
                                        this@GoalRegisterActivity
                                        , getString(R.string.server_unavailable_label)
                                        , Toast.LENGTH_SHORT).show()
                            }
                    )
        }
    }

    override fun onPause() {
        super.onPause()

        disposable?.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val i = Intent(this, if (isFromMain) AllTabFragment::class.java else GoalsTabFragment::class.java)
        setResult(Activity.RESULT_CANCELED, i)
        finish()
    }
}
