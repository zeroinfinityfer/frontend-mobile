package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_goal_update.*
import java.math.BigDecimal
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class GoalUpdateActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private var goalDefinitionValue: String? = null
    private var goalDescriptionValue: String? = null
    private var goalTargetValue: Double? = null
    private lateinit var goalDateCreatedValue: Date
    private var model: GoalModel? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }
    private lateinit var mInterstitialAd : InterstitialAd
    private var datePickerDialog: DatePickerDialog? = null
    private lateinit var formatter: DateFormat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goal_update)

        formatter = SimpleDateFormat(getString(R.string.date_format_formatter))

        goalDateCreatedUpdate.setOnClickListener { datePickerDialog?.show() }

        val bundle = intent.extras
        if (bundle != null) {
            model = bundle.getParcelable("model")
            goalDefinitionUpdate.setText(model?.name)
            goalTargetUpdate.setText(model?.targetAmount.toString())
            goalDescriptionUpdate.setText(model?.description)
            goalDateCreatedUpdate.text = formatter.format(model?.targetDate)
            goalDateCreatedValue = model?.targetDate!!

            val cal = Calendar.getInstance()
            cal.time = goalDateCreatedValue
            val currentYear = cal.get(Calendar.YEAR)
            val currentMonth = cal.get(Calendar.MONTH)
            val currentDay = cal.get(Calendar.DAY_OF_MONTH)

            datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
        }

        updateGoalButton.setOnClickListener(this)

        deleteGoalButton.setOnClickListener {
            financeAPI.deleteGoal(model?.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)
                                finish()

                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@GoalUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }

        cancelGoalButtonUpdate.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        goal_update_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        goalDateCreatedUpdate.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.goalDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }

    override fun onClick(v: View?) {
        var f = true
        if (goalDefinitionUpdate?.text.isNullOrEmpty()) {
            goalDefinitionUpdate?.error = getString(R.string.required_field_error)
            f = false
        } else
            this.goalDefinitionValue = goalDefinitionUpdate?.text.toString()

        when {
            goalTargetUpdate?.text.isNullOrEmpty() -> {
                goalTargetUpdate?.error = getString(R.string.required_field_error)
                f = false
            }
            goalTargetUpdate?.text.toString().toDouble() <= 0 -> {
                goalTargetUpdate?.error = getString(R.string.required_positive_number_error)
                f = false
            }
            else -> this.goalTargetValue = goalTargetUpdate?.text.toString().toDouble()
        }

        this.goalDescriptionValue = goalDescriptionUpdate?.text.toString()

        if (f) {
            model?.targetAmount = BigDecimal.valueOf(goalTargetValue!!)
            model?.targetDate = goalDateCreatedValue
            model?.description = goalDescriptionValue
            model?.name = goalDefinitionValue

            financeAPI.editGoal(model!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)

                                if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                    mInterstitialAd.adListener = object : AdListener() {
                                        override fun onAdClosed() {
                                            super.onAdClosed()
                                            finish()
                                        }
                                    }

                                    mInterstitialAd.show()
                                } else {
                                    finish()
                                }
                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@GoalUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
