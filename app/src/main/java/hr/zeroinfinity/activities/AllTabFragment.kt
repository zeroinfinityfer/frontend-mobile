package hr.zeroinfinity.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import hr.zeroinfinity.R
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.models.FinanceStatisticsModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.PairModel
import hr.zeroinfinity.services.XAxisValueResolver
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*


class AllTabFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var chart: CombinedChart
    private lateinit var progressBar: ProgressBar
    private lateinit var periodSpinner: Spinner
    private lateinit var noDataTextView: TextView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var xAxisValues: List<String> = mutableListOf()

    private var group: GroupModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        group = arguments?.getParcelable("group")
        return inflater.inflate(R.layout.fragment_all_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chart = view.findViewById(R.id.all_chart)
        progressBar = view.findViewById(R.id.progressBar_cyclic)
        periodSpinner = view.findViewById(R.id.period_spinner)
        noDataTextView = view.findViewById(R.id.no_data_text)
        periodSpinner = view.findViewById(R.id.period_spinner)
        swipeRefreshLayout = view.findViewById(R.id.swipe_container_all)

        ArrayAdapter.createFromResource(
                this.context!!,
                R.array.time_period_array,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            periodSpinner.adapter = adapter
            periodSpinner.setSelection(when (timeUnit) {
                "d" -> 0
                "m" -> 1
                "y" -> 2
                else -> 0
            })
        }

        periodSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val oldTimeUnit = timeUnit
                timeUnit = when (position) {
                    0 -> "d"
                    1 -> "m"
                    2 -> "y"
                    else -> "d"
                }

                if (timeUnit != oldTimeUnit) refresh()
            }
        }

        swipeRefreshLayout.setOnRefreshListener(this)

        refresh()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ExpensesTabFragment.registerExpenseRequestCode) {
                val newCategories = data?.extras?.getParcelableArrayList<CategoryModel>("categories")

                group?.categories = newCategories
            }

            refresh()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRefresh() {
        refresh()
    }

    private fun refresh() {
        if (group != null) {
            progressBar.visibility = View.VISIBLE
            val periodLength: Int = when (timeUnit) {
                "m" -> 1
                "d" -> 7
                "y" -> 1
                else -> 30
            }

            FinanceAPI.instance.getOverallStatistics(group?.id!!, timeUnit, periodLength, DATE_FORMAT.format(Date()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                if (it.barChartData != null && it.lineChartData != null && it.barChartData?.size!! > 0 && it.lineChartData?.size!! > 0) {
                                    noDataTextView.visibility = View.GONE
                                    chart.visibility = View.VISIBLE
                                    repaint(it)
                                } else {
                                    noDataTextView.visibility = View.VISIBLE
                                    chart.visibility = View.GONE
                                }

                                progressBar.visibility = View.GONE
                                swipeRefreshLayout.isRefreshing = false
                            },
                            {
                                progressBar.visibility = View.GONE
                                swipeRefreshLayout.isRefreshing = false

                                Log.e("HTTP ERROR", it.message)
                                Toast.makeText(this.context, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                            })
        } else {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun repaint(model: FinanceStatisticsModel) {
        chart.description?.isEnabled = false
        chart.setDrawGridBackground(false)
        chart.setDrawBarShadow(false)
        chart.isHighlightFullBarEnabled = false
        chart.legend.isEnabled = false

        chart.drawOrder = arrayOf(CombinedChart.DrawOrder.BAR, CombinedChart.DrawOrder.LINE)

        val xAxis = chart.xAxis
        xAxis?.position = XAxis.XAxisPosition.TOP
        xAxis?.axisMinimum = 0f
        xAxis?.granularity = 1f

        val dataC = CombinedData()
        dataC.setData(generateLineData(model.lineChartData!!))
        dataC.setData(generateBarData(model.barChartData!!))
        xAxis?.valueFormatter = IndexAxisValueFormatter(xAxisValues)

        xAxis?.axisMaximum = dataC.xMax + 0.25f
        xAxis?.textColor = Color.WHITE
        xAxis?.textSize = 6f
        chart.axisLeft?.textColor = Color.WHITE
        chart.axisRight?.textColor = Color.WHITE
        chart.data = dataC
        chart.data?.notifyDataChanged()
        chart.notifyDataSetChanged()
        chart.invalidate()
    }

    private fun generateLineData(lineChartData: List<PairModel>): LineData {
        xAxisValues = XAxisValueResolver.resolveValues(lineChartData)

        val data = LineData()
        val entries = ArrayList<Entry>()

        var i = 0
        var maxY = 0f
        for (it in lineChartData) {
            val value = it.value!!.toFloat()
            if (value > maxY) {
                maxY = value
            }

            entries.add(Entry(i.toFloat(), value, it.date))
            i += 1
        }

        val set = LineDataSet(entries, "")
        set.color = Color.WHITE
        set.lineWidth = 2.5f
        set.setCircleColor(Color.rgb(66, 134, 244))
        set.circleRadius = 5f
        set.fillColor = Color.LTGRAY
        set.mode = LineDataSet.Mode.LINEAR
        set.setValueTextColors(mutableListOf(Color.WHITE))
        set.setDrawValues(timeUnit != "m")
        set.setDrawCircles(timeUnit != "m")

        set.axisDependency = YAxis.AxisDependency.LEFT
        data.addDataSet(set)

        return data
    }

    private fun generateBarData(barChartData: List<PairModel>): BarData {
        val values = ArrayList<BarEntry>()
        val colors: MutableList<Int> = mutableListOf()

        var i = 0
        for (it in barChartData) {
            val barEntry = BarEntry(i.toFloat(), it.value!!.toFloat(), it.date)
            colors.add(if (it.value!! > 0) Color.rgb(65, 244, 83) else Color.rgb(232, 39, 71))
            values.add(barEntry)
            i += 1
        }

        val set: BarDataSet
        set = BarDataSet(values, "")
        set.colors = colors
        set.setDrawValues(false)
        set.axisDependency = YAxis.AxisDependency.LEFT

        return BarData(set)

    }

    companion object {
        private var timeUnit: String = "d"
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
    }
}
