package hr.zeroinfinity.activities

import android.app.Activity
import android.app.AlertDialog.Builder
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.DialogWithResult
import hr.zeroinfinity.services.retrofit.UserAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ForgotPasswordDialogFragment : DialogFragment(), DialogWithResult<Boolean> {
    private lateinit var input: TextInputEditText
    private var listener: DialogResultListener<Boolean>? = null
    private lateinit var mActivity: Activity

    override fun setListener(_listener: DialogResultListener<Boolean>) {
        listener = _listener
    }

    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@ForgotPasswordDialogFragment.context, R.anim.shake)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val createProjectAlert = Builder(activity)

        createProjectAlert.setTitle(getString(R.string.password_recovery_label))

        val inflater = activity!!.layoutInflater
        mActivity = activity!!
        val view = inflater.inflate(R.layout.forgot_password, null)

        input = view.findViewById(R.id.recovery_email)

        createProjectAlert.setView(view)
                .setPositiveButton(getString(R.string.recover_password_button_label)) { _, _ ->
                    if (input.text.isNullOrEmpty()) {
                        input.error = getString(R.string.required_field_error)
                        input.animation = shake
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(input.text).matches()) {
                        input.text?.clear()
                        input.error = getString(R.string.email_format_error)
                        input.animation = shake
                    } else {
                        UserAPI.instance.resetPassword(input.text?.toString()!!)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            listener?.onResult(true)
                                            dismiss()
                                        },
                                        {
                                            Log.e("HTTP ERROR", it.message)
                                            Toast.makeText(mActivity, mActivity.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                        })
                    }

                }
                .setNegativeButton(getString(R.string.cancel_button_label)) { dialog, _ ->
                    dialog.cancel()
                }

        return createProjectAlert.create()
    }
}
