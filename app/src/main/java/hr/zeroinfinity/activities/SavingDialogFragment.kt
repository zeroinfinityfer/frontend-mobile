package hr.zeroinfinity.activities

import android.app.Activity
import android.app.AlertDialog.Builder
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.DialogFragment
import android.util.Log
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.DialogWithResult
import hr.zeroinfinity.models.SavingEntryModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


class SavingDialogFragment : DialogFragment(), DialogWithResult<SavingEntryModel> {
    private lateinit var input: TextInputEditText
    private var listener: DialogResultListener<SavingEntryModel>? = null
    private lateinit var mActivity: Activity

    override fun setListener(_listener: DialogResultListener<SavingEntryModel>) {
        listener = _listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val createProjectAlert = Builder(activity)

        createProjectAlert.setTitle(getString(R.string.add_amount_to_savings_button_label))

        val inflater = activity!!.layoutInflater
        mActivity = activity!!
        val view = inflater.inflate(R.layout.fragment_dialog_saving, null)

        input = view.findViewById(R.id.addToSavingInput)
        createProjectAlert.setCancelable(false)
        createProjectAlert.setView(view)
                .setPositiveButton(R.string.add_button_label) { _, _ ->
                    if (input.text.isNullOrEmpty()) {
                        Toast.makeText(activity, "Krivi unos: polje za unos je ostalo prazno !", Toast.LENGTH_LONG).show()
                    } else {
                        if (input.text.toString().startsWith(".")) {
                            Toast.makeText(activity, "Krivi unos: izraz ne smije započinjati točkom !", Toast.LENGTH_LONG).show()
                        } else {
                            isCancelable = true
                            val savingInput = input.text?.toString()?.toBigDecimal()!!
                            val groupId = UUID.fromString(arguments?.getString("groupId"))

                            val model = SavingEntryModel(groupId, savingInput, Date())

                            FinanceAPI.instance.addToSaving(model)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(
                                            {
                                                listener?.onResult(model)
                                                dismiss()
                                            },
                                            {
                                                Log.e("HTTP ERROR", it.message)
                                                Toast.makeText(mActivity, mActivity.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                            })
                        }
                    }

                }
                .setNegativeButton(getString(R.string.cancel_button_label)) { dialog, _ ->
                    dialog.cancel()
                }

        return createProjectAlert.create()
    }
}
