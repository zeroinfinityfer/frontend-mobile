package hr.zeroinfinity.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.InvitesListAdapter
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.Listener
import hr.zeroinfinity.models.InviteModel
import hr.zeroinfinity.services.UserService
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InvitesManagementActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener, Listener<Boolean> {

    private var invitesArray: MutableLiveData<MutableList<InviteModel>> = MutableLiveData()
    private lateinit var invitesListView: ListView
    private lateinit var noDataTextView: TextView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var isInviteAccepted: Boolean = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invites_management)

        invitesListView = findViewById(R.id.invites_list_view)
        invitesListView.setOnTouchListener { v, _ ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            return@setOnTouchListener false
        }

        noDataTextView = findViewById(R.id.no_data_text)
        swipeRefreshLayout = findViewById(R.id.swipe_container_invites)

        invitesListView.adapter = InvitesListAdapter(this, R.layout.activity_invites_management, invitesArray)
        invitesArray.observe(this, Observer<MutableList<InviteModel>> {
            if (it?.size == 0) {
                noDataTextView.visibility = View.VISIBLE
                invitesListView.visibility = View.GONE
            } else {
                noDataTextView.visibility = View.GONE
                invitesListView.visibility = View.VISIBLE
            }

            invitesListView.adapter = InvitesListAdapter(this, R.layout.activity_invites_management, invitesArray)
        })

        swipeRefreshLayout.setOnRefreshListener(this)

        swipeRefreshLayout.isRefreshing = true
        supportActionBar?.title = getString(R.string.invites_label)

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        if (UserService.jwtToken != null) {
            onRefresh()
        } else {
            initApk()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initApk() {
        UserService.initService(PreferenceManager.getDefaultSharedPreferences(this@InvitesManagementActivity), object : Listener<Boolean?> {
                    override fun update(data: Boolean?) {
                        runOnUiThread {
                            if (data == null) {
                                Snackbar.make(this@InvitesManagementActivity.swipeRefreshLayout, getString(R.string.server_unavailable_label), Snackbar.LENGTH_INDEFINITE)
                                        .setAction(getString(R.string.retry_button_label)) {
                                            initApk()
                                        }.show()
                            } else if (!data) {
                                AlertDialog.Builder(this@InvitesManagementActivity)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle(getString(R.string.not_logged_in_label))
                                        .setMessage(getString(R.string.log_in_required_label))
                                        .setPositiveButton(getString(R.string.sign_in_label)) { _, _ ->
                                            run {
                                                val intent = Intent(this@InvitesManagementActivity, LoginActivity::class.java)
                                                intent.flags = intent.flags or Intent.FLAG_ACTIVITY_NO_HISTORY
                                                startActivity(intent)
                                            }
                                        }
                                        .setNegativeButton(getString(R.string.cancel_button_label)) { _, _ -> moveTaskToBack(true) }
                                        .show()
                            } else {
                                onRefresh()
                            }
                        }
                    }
                })
    }

    override fun onRefresh() {
        GroupAPI.instance.invites
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            invitesArray.postValue(it ?: mutableListOf())
                            swipeRefreshLayout.isRefreshing = false
                        },
                        {
                            Log.e("HTTP ERROR", it.message)

                            invitesArray.postValue(mutableListOf())
                            swipeRefreshLayout.isRefreshing = false
                            Toast.makeText(this, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                        })
    }

    override fun onBackPressed() {
        val i = Intent(this, MainActivity::class.java)
        setResult(Activity.RESULT_OK, i)
        super.onBackPressed()
    }

    override fun update(data: Boolean) {
        if (data) isInviteAccepted = true
    }
}
