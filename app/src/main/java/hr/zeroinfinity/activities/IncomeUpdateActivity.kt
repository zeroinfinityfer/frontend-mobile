package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.IncomeModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_income_update.*
import java.math.BigDecimal
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class IncomeUpdateActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private var incomeAmountValue: Double? = null
    private var incomeDescriptionValue: String? = null
    private lateinit var incomeDateCreatedValue: Date
    private var model: IncomeModel? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }
    private lateinit var mInterstitialAd : InterstitialAd
    private var datePickerDialog: DatePickerDialog? = null
    private lateinit var formatter: DateFormat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_income_update)

        formatter = SimpleDateFormat(getString(R.string.date_format_formatter))
        incomeDateCreatedUpdate.setOnClickListener { datePickerDialog?.show() }

        val bundle = intent.extras
        if (bundle != null) {
            model = bundle.getParcelable("model")
            incomeAmountUpdate.setText(model?.amount.toString())
            incomeDescriptionUpdate.setText(model?.description)
            incomeDateCreatedUpdate.text = formatter.format(model?.dateCreated)
            incomeDateCreatedValue = model?.dateCreated!!


            val cal = Calendar.getInstance()
            cal.time = incomeDateCreatedValue
            val currentYear = cal.get(Calendar.YEAR)
            val currentMonth = cal.get(Calendar.MONTH)
            val currentDay = cal.get(Calendar.DAY_OF_MONTH)

            datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis() - 1000
        }

        updateIncomeButton.setOnClickListener(this)

        deleteIncomeButton.setOnClickListener {
            financeAPI.deleteIncome(model?.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)
                                finish()
                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@IncomeUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }
        cancelIncomeButtonUpdate.setOnClickListener {
            val i = Intent(this, IncomesTabFragment::class.java)
            setResult(Activity.RESULT_CANCELED, i)
            finish()
        }

        income_update_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        incomeDateCreatedUpdate.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.incomeDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }

    override fun onClick(v: View?) {
        var f = true

        when {
            incomeAmountUpdate?.text.isNullOrEmpty() -> {
                incomeAmountUpdate?.error = getString(R.string.required_field_error)
                f = false
            }
            incomeAmountUpdate?.text.toString().toDouble() <= 0 -> {
                incomeAmountUpdate?.error = getString(R.string.required_positive_number_error)
                f = false
            }
            else -> this.incomeAmountValue = incomeAmountUpdate?.text.toString().toDouble()
        }

        this.incomeDescriptionValue = incomeDescriptionUpdate?.text.toString()

        if (f) {
            model?.amount = BigDecimal.valueOf(this.incomeAmountValue!!)
            model?.description = this.incomeDescriptionValue
            model?.dateCreated = this.incomeDateCreatedValue

            financeAPI.editIncome(model!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)

                                if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                    mInterstitialAd.adListener = object : AdListener() {
                                        override fun onAdClosed() {
                                            super.onAdClosed()
                                            finish()
                                        }
                                    }

                                    mInterstitialAd.show()
                                } else {
                                    finish()
                                }

                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@IncomeUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
