package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_expense_register.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import java.util.stream.Collectors


class ExpenseRegisterActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener, DialogResultListener<CategoryModel> {

    private var expenseAmountValue: Double? = null
    private var expenseDescriptionValue: String? = null
    private var expenseDateCreatedValue: Date? = Date()
    private var categorySelected: Int = 0
    private var categories: MutableList<String> = mutableListOf()
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var groupId: UUID
    private lateinit var mInterstitialAd : InterstitialAd
    private var isFromMain: Boolean = false
    private var disposable: Disposable? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }


    private lateinit var categoryDialogFragment: NewCategoryDialogFragment
    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@ExpenseRegisterActivity, R.anim.shake)
    }
    private lateinit var categoriesFull: MutableList<CategoryModel>


    private var datePickerDialog: DatePickerDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expense_register)

        expenseDateCreated.text = SimpleDateFormat(getString(R.string.date_format_formatter)).format(expenseDateCreatedValue)

        categoryDialogFragment = NewCategoryDialogFragment()
        categoryDialogFragment.setListener(this)
        val cal = Calendar.getInstance()
        cal.time = expenseDateCreatedValue
        val currentYear = cal.get(Calendar.YEAR)
        val currentMonth = cal.get(Calendar.MONTH)
        val currentDay = cal.get(Calendar.DAY_OF_MONTH)

        datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
        datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis() - 1000

        expenseDateCreated.setOnClickListener {
            expenseDateCreated.error = null
            datePickerDialog?.show()
        }

        val bundle = intent.extras
        if (bundle != null) {
            groupId = bundle.get("groupId") as UUID
            isFromMain = bundle.get("isFromMain") as Boolean
            categoriesFull = bundle.get("categories") as MutableList<CategoryModel>
            this.categories = categoriesFull.stream().map { it.name }.collect(Collectors.toList())
        }

        adapter = ArrayAdapter(this, R.layout.custom_list_item, this.categories)
        categoryListView.adapter = adapter
        categoryListView.setOnTouchListener { v, _ ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            return@setOnTouchListener false
        }

        addExpenseButton.setOnClickListener(this)
        cancelExpenseButton.setOnClickListener {
            val i = Intent(this, if (isFromMain) AllTabFragment::class.java else ExpensesTabFragment::class.java)
            setResult(Activity.RESULT_CANCELED, i)
            finish()
        }


        categoryListView.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            this.categorySelected = position
            categoryListView.setItemChecked(position, true)
        }

        addNewCategoryButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("groupId", groupId.toString())
            categoryDialogFragment.arguments = bundle
            categoryDialogFragment.show(supportFragmentManager, "fragment_dialog")
        }

        expense_register_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        expenseDateCreated.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.expenseDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }


    override fun onBackPressed() {
        val i = Intent(this, if (isFromMain) AllTabFragment::class.java else ExpensesTabFragment::class.java)
        setResult(Activity.RESULT_CANCELED, i)
        finish()
    }

    override fun onClick(v: View?) {
        var f = true

        when {
            expenseAmount?.text.isNullOrEmpty() -> {
                expenseAmount?.error = getString(R.string.required_field_error)
                expenseAmount?.animation = shake
                f = false
            }
            expenseAmount?.text.toString().toDouble() <= 0 -> {
                expenseAmount?.error = getString(R.string.required_positive_number_error)
                expenseAmount?.animation = shake
                f = false
            }
            else -> this.expenseAmountValue = expenseAmount?.text.toString().toDouble()
        }

        this.expenseDescriptionValue = expenseDescription?.text.toString()

        if (f) {
            val model = ExpenseModel()
            model.amount = BigDecimal.valueOf(expenseAmountValue!!)
            model.dateCreated = expenseDateCreatedValue
            model.description = expenseDescriptionValue
            model.groupId = groupId
            model.categoryId = categoriesFull[categorySelected].id

            disposable = financeAPI.registerExpense(model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { r ->
                                run {
                                    val i = Intent(this, if (isFromMain) AllTabFragment::class.java else ExpensesTabFragment::class.java)
                                    i.putParcelableArrayListExtra("categories", categoriesFull as ArrayList<CategoryModel>)
                                    setResult(Activity.RESULT_OK, i)

                                    if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                        mInterstitialAd.adListener = object : AdListener() {
                                            override fun onAdClosed() {
                                                super.onAdClosed()
                                                finish()
                                            }
                                        }

                                        mInterstitialAd.show()
                                    } else {
                                        finish()
                                    }
                                }
                            },
                            { e ->
                                Log.d("HTTP ERROR", e.message)
                                Toast.makeText(
                                        this@ExpenseRegisterActivity
                                        , getString(R.string.server_unavailable_label)
                                        , Toast.LENGTH_SHORT).show()
                            }
                    )
        }
    }

    override fun onPause() {
        super.onPause()

        disposable?.dispose()
    }

    override fun onResult(result: CategoryModel) {
        categoriesFull.add(result)

        adapter.add(result.name!!)
        categoryListView.invalidate()
    }
}