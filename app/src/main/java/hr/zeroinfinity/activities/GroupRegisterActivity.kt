package hr.zeroinfinity.activities

import android.app.Activity
import android.content.Context
import android.database.DataSetObserver
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.GroupMemberInviteListAdapter
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_group_register.*


class GroupRegisterActivity : AppCompatActivity() {

    private var userEmailList: MutableList<String> = ArrayList()
    private lateinit var adapter: GroupMemberInviteListAdapter
    private var disposable: Disposable? = null
    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@GroupRegisterActivity, R.anim.shake)
    }
    private lateinit var mInterstitialAd : InterstitialAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_register)

        adapter = GroupMemberInviteListAdapter(this, R.layout.user_email_list_item, userEmailList)
        adapter.registerDataSetObserver(object : DataSetObserver() {
            override fun onChanged() {
                ListViewHelper.setListViewHeightBasedOnChildren(user_email_list_view)
            }
        })

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        user_email_list_view.adapter = adapter
//        user_email_list_view.setOnTouchListener { v, _ ->
//            v.parent.requestDisallowInterceptTouchEvent(true)
//            return@setOnTouchListener false
//        }

        add_user_to_list_button.setOnClickListener {
            addToMailList()
        }

        send_button.setOnClickListener {
            if (!email_input.text?.trim()?.isEmpty()!!) {
                if (!addToMailList()) {
                    return@setOnClickListener
                }
            }

            val groupName = name_input.text.toString()
            val description = description_input.text.toString()

            if (validName(groupName)) {
                sendRegistration(groupName, description)
            }
        }
        group_register_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        val currencyAdapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                resources.getStringArray(R.array.currency_codes))
        val pos = currencyAdapter.getPosition("EUR")
        currency_spinner.adapter = currencyAdapter
        currency_spinner.setSelection(pos)

        ListViewHelper.setListViewHeightBasedOnChildren(user_email_list_view)

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)
    }

    private fun addToMailList(): Boolean {
        val userEmail: String = email_input.text.toString().trim()

        return if (validEmail(userEmail)) {
            userEmailList.add(userEmail)
            email_input.text?.clear()
            adapter.notifyDataSetChanged()

            true
        } else {
            email_input.text?.clear()
            email_input.error = getString(R.string.email_format_error)
            email_input.animation = shake

            false
        }
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

    private fun validEmail(userEmail: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()
    }

    private fun validName(groupName: String): Boolean {

        if (groupName.isEmpty()) {
            name_input.text?.clear()
            name_input.error = getString(R.string.required_field_error)
            name_input.animation = shake
            return false
        } else if (groupName.length > 128) {
            name_input.text?.clear()
            name_input.error = getString(R.string.input_too_long_error)
            name_input.animation = shake
            return false
        }
        return true
    }

    private fun sendRegistration(groupName: String, description: String){
        val groupModel = GroupModel()
        groupModel.name = groupName
        groupModel.description = description
        groupModel.currency = currency_spinner.selectedItem as String
        groupModel.invitationEmails = userEmailList

        disposable = GroupAPI.instance.registerGroup(groupModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { r ->
                            setResult(Activity.RESULT_OK)
                            if(mInterstitialAd.isLoaded) {
                                mInterstitialAd.adListener = object : AdListener() {
                                    override fun onAdClosed() {
                                        super.onAdClosed()
                                        finish()
                                    }
                                }

                                mInterstitialAd.show()
                            } else {
                                finish()
                            }
                        },
                        { e ->
                            Log.d("HTTP ERROR", e.message)
                            Toast.makeText(
                                    this@GroupRegisterActivity
                                    , getString(R.string.server_unavailable_label)
                                    , Toast.LENGTH_SHORT).show()
                        }
                )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList("data", userEmailList as java.util.ArrayList<String>)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null && user_email_list_view != null) {
            userEmailList = savedInstanceState.getSerializable("data") as ArrayList<String>
            adapter = GroupMemberInviteListAdapter(this, R.layout.user_email_list_item, userEmailList)
            user_email_list_view.adapter = adapter
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }
}