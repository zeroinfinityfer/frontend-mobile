package hr.zeroinfinity.activities

import android.view.View
import android.view.ViewGroup
import android.widget.ListView

object ListViewHelper {
    fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter ?: return
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.width, View.MeasureSpec.UNSPECIFIED)

        var totalHeight = 0
        var view: View? = null
        for (i in 0..(listAdapter.count - 1)) {
            view = listAdapter.getView(i, view, listView)
            if (i == 0) view.layoutParams = ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT)

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += view.measuredHeight
        }

        val params = listView.layoutParams

        params.height = totalHeight + (listView.dividerHeight *
                (listAdapter.count - 1))

        listView.layoutParams = params
        listView.requestLayout()
    }
}