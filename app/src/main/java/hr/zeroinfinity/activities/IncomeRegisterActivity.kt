package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.IncomeModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_income_register.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

class IncomeRegisterActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private var incomeAmountValue: Double? = null
    private var incomeDescriptionValue: String? = null
    private var incomeDateCreatedValue: Date = Date()
    private lateinit var groupId: UUID
    private var isFromMain: Boolean = false
    private var disposable: Disposable? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }
    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@IncomeRegisterActivity, R.anim.shake)
    }

    private lateinit var mInterstitialAd : InterstitialAd
    private var datePickerDialog: DatePickerDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_income_register)
        incomeDateCreated.text = SimpleDateFormat(getString(R.string.date_format_formatter)).format(incomeDateCreatedValue)

        val cal = Calendar.getInstance()
        cal.time = incomeDateCreatedValue
        val currentYear = cal.get(Calendar.YEAR)
        val currentMonth = cal.get(Calendar.MONTH)
        val currentDay = cal.get(Calendar.DAY_OF_MONTH)

        datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
        datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis() - 1000

        incomeDateCreated.setOnClickListener {
            incomeDateCreated.error = null
            datePickerDialog?.show()
        }

        val bundle = intent.extras
        if (bundle != null) {
            groupId = bundle.get("groupId") as UUID
            isFromMain = bundle.get("isFromMain") as Boolean
        }

        addIncomeButton.setOnClickListener(this)
        cancelIncomeButton.setOnClickListener {
            val i = Intent(this, if (isFromMain!!) AllTabFragment::class.java else IncomesTabFragment::class.java)
            setResult(Activity.RESULT_CANCELED, i)
            finish()
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        incomeDateCreated.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.incomeDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }


    override fun onClick(v: View?) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        var f = true

        when {
            incomeAmount?.text.isNullOrEmpty() -> {
                incomeAmount?.error = getString(R.string.required_field_error)
                incomeAmount?.animation = shake
                f = false
            }
            incomeAmount?.text.toString().toDouble() <= 0 -> {
                incomeAmount?.error = getString(R.string.required_positive_number_error)
                incomeAmount?.animation = shake
                f = false
            }
            else -> this.incomeAmountValue = incomeAmount?.text.toString().toDouble()
        }

        this.incomeDescriptionValue = incomeDescription?.text.toString()

        if (f) {
            val model = IncomeModel()
            model.amount = BigDecimal.valueOf(this.incomeAmountValue!!)
            model.description = this.incomeDescriptionValue
            model.groupId = this.groupId
            model.dateCreated = this.incomeDateCreatedValue

            disposable = financeAPI.registerIncome(model)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { r ->
                                run {
                                    val i = Intent(this, if (isFromMain) AllTabFragment::class.java else IncomesTabFragment::class.java)

                                    setResult(Activity.RESULT_OK, i)

                                    if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                        mInterstitialAd.adListener = object : AdListener() {
                                            override fun onAdClosed() {
                                                super.onAdClosed()
                                                finish()
                                            }
                                        }

                                        mInterstitialAd.show()
                                    } else {
                                        finish()
                                    }
                                }
                            },
                            { e ->
                                Log.d("HTTP ERROR", e.message)
                                Toast.makeText(
                                        this@IncomeRegisterActivity
                                        , getString(R.string.server_unavailable_label)
                                        , Toast.LENGTH_SHORT).show()
                            }
                    )
        }
    }

    override fun onPause() {
        super.onPause()

        disposable?.dispose()
    }

    override fun onBackPressed() {
        val i = Intent(this, if (isFromMain) AllTabFragment::class.java else IncomesTabFragment::class.java)
        setResult(Activity.RESULT_CANCELED, i)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
