package hr.zeroinfinity.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import hr.zeroinfinity.R
import hr.zeroinfinity.common.Listener
import hr.zeroinfinity.services.UserService
import hr.zeroinfinity.services.retrofit.InfoAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        val animation = AnimationUtils.loadAnimation(this, R.anim.spin)
        val logo = findViewById<ImageView>(R.id.logo)

        logo.animation = animation
        logo.animation.repeatCount = Animation.INFINITE
        logo.animation.interpolator = AccelerateDecelerateInterpolator()
        logo.animation.start()
    }

    fun initApk() {
        UserService.initService(PreferenceManager.getDefaultSharedPreferences(this@TestActivity), object : Listener<Boolean?> {
            override fun update(data: Boolean?) {
                if (data == null) {
                    runOnUiThread {
                        Snackbar.make(this@TestActivity.findViewById(R.id.test_activity_view), getString(R.string.server_unavailable_label), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.retry_button_label)) {
                                    initApk()
                                }.show()
                    }
                } else if (!data) {
                    startActivity(Intent(this@TestActivity, LoginActivity::class.java), null)
                } else {
                    startActivity(Intent(this@TestActivity, MainActivity::class.java), null)
                }

            }
        })
    }

    private fun checkVersion(){
        InfoAPI.instance.getVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {r -> run{
                            if(r == this.packageManager.getPackageInfo(this.packageName, 0).versionName) initApk()
                            else {
                                AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Application is out of date")
                                        .setMessage("You must perform an application update!")
                                        .setPositiveButton("Update") { _, _ ->
                                            try {
                                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                                            } catch (e: ActivityNotFoundException) {
                                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
                                            }

                                            finish()
                                        }
                                        .setNegativeButton("Exit") { _, _ ->  moveTaskToBack(true) }
                                        .show()
                            }
                        }},
                        {Snackbar.make(this@TestActivity.findViewById(R.id.test_activity_view), getString(R.string.server_unavailable_label), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.retry_button_label)) {
                                    checkVersion()
                                }.show()}
                )
    }

    override fun onResume() {
        checkVersion()
        super.onResume()
    }
}
