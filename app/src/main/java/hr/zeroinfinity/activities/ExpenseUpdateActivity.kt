package hr.zeroinfinity.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import com.google.android.gms.ads.*
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.RandomObject
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.models.ExpenseModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_expense_update.*
import java.math.BigDecimal
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.stream.Collectors


class ExpenseUpdateActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, View.OnClickListener, DialogResultListener<CategoryModel> {

    private lateinit var imm: InputMethodManager
    private var expenseAmountValue: Double? = null
    private var expenseDescriptionValue: String? = null
    private lateinit var expenseDateCreatedValue: Date
    private var categorySelected: Int = 0
    private var categories: MutableList<String> = mutableListOf()
    private lateinit var adapter: ArrayAdapter<String>
    private var model: ExpenseModel? = null
    private val financeAPI: FinanceAPI by lazy {
        FinanceAPI.instance
    }
    private lateinit var mInterstitialAd : InterstitialAd
    private lateinit var categoriesFull: MutableList<CategoryModel>
    private lateinit var formatter: DateFormat
    private lateinit var categoryDialogFragment: NewCategoryDialogFragment


    private var datePickerDialog: DatePickerDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expense_update)
        formatter = SimpleDateFormat(getString(R.string.date_format_formatter))

        imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        expenseDateCreatedUpdate.setOnClickListener { datePickerDialog?.show() }
        categoryDialogFragment = NewCategoryDialogFragment()
        categoryDialogFragment.setListener(this)

        val bundle = intent.extras
        if (bundle != null) {
            model = bundle.getParcelable("model")
            categoriesFull = bundle.get("categories") as MutableList<CategoryModel>
            this.categories = categoriesFull.stream().map { it.name }.collect(Collectors.toList())
            adapter = ArrayAdapter(this, R.layout.custom_list_item, this.categories)
            categoryListViewUpdate.adapter = adapter

            var selectedIndex = -1
            for (i in 0 until categoriesFull.size) {
                if (categoriesFull[i].id!! == model?.categoryId) {
                    selectedIndex = i
                    break
                }
            }

            if (selectedIndex >= 0) {
                categoryListViewUpdate.setItemChecked(selectedIndex, true)
                categoryListViewUpdate.invalidate()
            }

            expenseAmountUpdate.setText(model?.amount.toString())
            expenseDescriptionUpdate.setText(model?.description)
            expenseDateCreatedUpdate.text = formatter.format(model?.dateCreated)
            expenseDateCreatedValue = model?.dateCreated!!

            val cal = Calendar.getInstance()
            cal.time = expenseDateCreatedValue
            val currentYear = cal.get(Calendar.YEAR)
            val currentMonth = cal.get(Calendar.MONTH)
            val currentDay = cal.get(Calendar.DAY_OF_MONTH)

            datePickerDialog = DatePickerDialog(this, this, currentYear, currentMonth, currentDay)
            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis() - 1000
        }

        updateExpenseButton.setOnClickListener(this)

        deleteExpenseButton.setOnClickListener {
            financeAPI.deleteExpense(model?.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)
                                finish()

                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@ExpenseUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }

        cancelExpenseButtonUpdate.setOnClickListener {
            val i = Intent(this, ExpensesTabFragment::class.java)
            setResult(Activity.RESULT_CANCELED, i)
            finish()
        }

        addNewCategoryButtonUpdate.setOnClickListener {
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            val bundle = Bundle()
            bundle.putString("groupId", model?.groupId.toString())
            categoryDialogFragment.arguments = bundle
            categoryDialogFragment.show(supportFragmentManager, "fragment_dialog")
        }

        categoryListViewUpdate.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            this.categorySelected = position
            categoryListViewUpdate.setItemChecked(position, true)
        }

        categoryListViewUpdate.setOnTouchListener { v, _ ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            return@setOnTouchListener false
        }
        expense_update_view.setOnClickListener {
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.AD_MOB_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        expenseDateCreatedUpdate.text = String.format(getString(R.string.date_format_placeholder, dayOfMonth, month + 1, year))
        this.expenseDateCreatedValue = GregorianCalendar(year, month, dayOfMonth).time
    }

    override fun onBackPressed() {
        val i = Intent(this, MainActivity::class.java)
        setResult(Activity.RESULT_CANCELED, i)
        finish()
    }

    override fun onClick(v: View?) {
        var f = true

        when {
            expenseAmountUpdate?.text.isNullOrEmpty() -> {
                expenseAmountUpdate?.error = getString(R.string.required_field_error)
                f = false
            }
            expenseAmountUpdate?.text.toString().toDouble() <= 0 -> {
                expenseAmountUpdate?.error = getString(R.string.required_positive_number_error)
                f = false
            }
            else -> this.expenseAmountValue = expenseAmountUpdate?.text.toString().toDouble()
        }

        this.expenseDescriptionValue = expenseDescriptionUpdate?.text.toString()

        if (f) {
            model?.amount = BigDecimal.valueOf(expenseAmountValue!!)
            model?.dateCreated = expenseDateCreatedValue
            model?.description = expenseDescriptionValue
            model?.categoryId = categoriesFull[categorySelected].id

            financeAPI.editExpense(model!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                val i = Intent(this, MainActivity::class.java)
                                setResult(Activity.RESULT_OK, i)

                                if(mInterstitialAd.isLoaded && RandomObject.getBoolean(BuildConfig.AD_PERCENTAGE)) {
                                    mInterstitialAd.adListener = object : AdListener() {
                                        override fun onAdClosed() {
                                            super.onAdClosed()
                                            finish()
                                        }
                                    }

                                    mInterstitialAd.show()
                                } else {
                                    finish()
                                }
                            },
                            { e ->
                                run {
                                    Log.d("HTTP ERROR", e?.message)
                                    Toast.makeText(this@ExpenseUpdateActivity, getString(R.string.server_unavailable_label), Toast.LENGTH_LONG).show()
                                }
                            }
                    )
        }
    }

    override fun onResult(result: CategoryModel) {
        categoriesFull.add(result)

        adapter.add(result.name!!)
        categoryListViewUpdate.invalidate()
    }
}