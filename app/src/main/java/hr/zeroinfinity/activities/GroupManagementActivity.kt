package hr.zeroinfinity.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.GroupMembesListAdapter
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class GroupManagementActivity : AppCompatActivity() {

    private lateinit var nameTextInput: TextInputEditText
    private lateinit var descriptionTextInput: TextInputEditText
    private lateinit var editBtn1: Button
    private var isEditBtn1: Boolean = false
    private var oldName: String? = null
    private var oldDescription: String? = null
    private lateinit var editBtn2: Button
    private var isEditBtn2: Boolean = false
    private lateinit var membersListView: ListView
    private lateinit var group: GroupModel
    private lateinit var adapter: GroupMembesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_management)
        nameTextInput = findViewById(R.id.name_input)
        descriptionTextInput = findViewById(R.id.description_input)
        editBtn1 = findViewById(R.id.edit_group_name)
        editBtn2 = findViewById(R.id.edit_group_description)

        if (intent != null) {
            group = intent.getParcelableExtra("group")

            nameTextInput.setText(group.name)
            descriptionTextInput.setText(group.description)
        }

        disableEditText(nameTextInput)
        disableEditText(descriptionTextInput)
        if (group.activeUserIsAdmin) {
            editBtn1.visibility = View.VISIBLE
            editBtn1.setOnClickListener {
                if (isEditBtn1) {
                    if (nameTextInput.text.isNullOrEmpty()) {
                        nameTextInput.error = getString(R.string.required_field_error)

                    } else if (nameTextInput.text?.length!! > 128) {
                        nameTextInput.text?.clear()
                        nameTextInput.error = getString(R.string.input_too_long_error)
                    } else {
                        group.name = nameTextInput.text?.toString()
                        GroupAPI.instance.editGroup(group)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    isEditBtn1 = false
                                    editBtn1.background = getDrawable(android.R.drawable.ic_menu_edit)
                                    disableEditText(nameTextInput)
                                }, {
                                    isEditBtn1 = false
                                    nameTextInput.setText(oldName)
                                    disableEditText(nameTextInput)
                                    editBtn1.background = getDrawable(android.R.drawable.ic_menu_edit)
                                    group.name = oldName

                                    Log.e("HTTP ERROR", it.message)
                                    Toast.makeText(this, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                })
                    }
                } else {
                    oldName = nameTextInput.text?.toString()
                    isEditBtn1 = true
                    editBtn1.background = getDrawable(android.R.drawable.ic_menu_save)
                    enableEditText(nameTextInput)
                }
            }

            editBtn2.visibility = View.VISIBLE
            editBtn2.setOnClickListener {
                if (isEditBtn2) {
                    group.description = descriptionTextInput.text?.toString()
                    GroupAPI.instance.editGroup(group)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        isEditBtn2 = false
                                        editBtn2.background = getDrawable(android.R.drawable.ic_menu_edit)
                                        disableEditText(descriptionTextInput)
                                    },
                                    {
                                        Log.e("HTTP ERROR", it.message)

                                        isEditBtn2 = false
                                        descriptionTextInput.setText(oldName)
                                        disableEditText(descriptionTextInput)
                                        editBtn2.background = getDrawable(android.R.drawable.ic_menu_edit)
                                        group.description = oldDescription
                                        Toast.makeText(this, getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                    })
                } else {
                    oldDescription = descriptionTextInput.text?.toString()
                    isEditBtn2 = true
                    editBtn2.background = getDrawable(android.R.drawable.ic_menu_save)
                    enableEditText(descriptionTextInput)
                }
            }
        }

        membersListView = findViewById(R.id.members_list)
        for (member in group.members!!) {
            member.isGroupAdmin = group.admins?.contains(member)!!
        }

        adapter = GroupMembesListAdapter(this, R.layout.activity_group_management, group)
        membersListView.adapter = adapter

        findViewById<Button>(R.id.add_user_to_list_button)
                .setOnClickListener {
                    val i = Intent(this, GroupInviteActivity::class.java)

                    i.putExtra("groupId", group.id.toString())

                    startActivity(i)
                }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)
    }

    override fun onBackPressed() {
        val i = Intent(this, MainActivity::class.java)
        setResult(Activity.RESULT_OK, i)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun disableEditText(editText: EditText) {
        editText.isFocusable = false
        editText.isCursorVisible = false
    }

    private fun enableEditText(editText: EditText) {
        editText.isFocusableInTouchMode = true
        editText.isCursorVisible = true
    }
}
