package hr.zeroinfinity.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import hr.zeroinfinity.BuildConfig
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.GroupMemberInviteListAdapter
import hr.zeroinfinity.common.AdListenerImpl
import hr.zeroinfinity.models.GroupInviteModel
import hr.zeroinfinity.services.retrofit.GroupAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_group_invite.*
import java.util.*


class GroupInviteActivity : AppCompatActivity() {

    private var userEmailList: MutableList<String> = ArrayList()
    private lateinit var adapter: GroupMemberInviteListAdapter
    private var disposable: Disposable? = null
    private val shake: Animation by lazy {
        AnimationUtils.loadAnimation(this@GroupInviteActivity, R.anim.shake)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_invite)


        val groupId = UUID.fromString(intent.getStringExtra("groupId"))
        adapter = GroupMemberInviteListAdapter(this, R.layout.user_email_list_item, userEmailList)

        user_email_list_view.adapter = adapter
        user_email_list_view.setOnTouchListener { v, _ ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            return@setOnTouchListener false
        }
        add_user_to_list_button.setOnClickListener {
            addToMailList()
        }

        send_button.setOnClickListener {
            if (!email_input.text?.trim()?.isEmpty()!!) addToMailList()

            if (userEmailList.isEmpty()) {
                Toast.makeText(this, getString(R.string.email_list_empty), Toast.LENGTH_SHORT).show()
            } else {
                val groupInviteModel = GroupInviteModel(groupId, userEmailList)

                disposable = GroupAPI.instance
                        .inviteMembers(groupInviteModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    val i = Intent(this, MainActivity::class.java) //TODO promijeni na prosli ekran
                                    setResult(Activity.RESULT_OK, i)
                                    finish()
                                },
                                { e ->
                                    Log.d("HTTP ERROR", e.message)
                                    Toast.makeText(
                                            this@GroupInviteActivity
                                            , getString(R.string.server_unavailable_label)
                                            , Toast.LENGTH_SHORT).show()
                                }
                        )
            }
        }

        group_invite_view.setOnClickListener {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }

        supportActionBar?.title = ""

        MobileAds.initialize(this, BuildConfig.AD_MOB_ID)
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.adListener = AdListenerImpl

        mAdView.loadAd(adRequest)
    }

    override fun onPause() {
        super.onPause()

        disposable?.dispose()
    }

    private fun validEmail(userEmail: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList("data", userEmailList as java.util.ArrayList<String>)
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null && user_email_list_view != null) {
            userEmailList = savedInstanceState.getSerializable("data") as ArrayList<String>
            adapter = GroupMemberInviteListAdapter(this, R.layout.user_email_list_item, userEmailList)
            user_email_list_view.adapter = adapter
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun addToMailList(): Boolean {
        val userEmail: String = email_input.text.toString().trim()

        if (validEmail(userEmail)) {
            userEmailList.add(userEmail)
            email_input.text?.clear()
            adapter.notifyDataSetChanged()

            return true
        } else {
            email_input.text?.clear()
            email_input.error = getString(R.string.email_format_error)
            email_input.animation = shake

            return false
        }
    }
}