package hr.zeroinfinity.activities

import android.app.Activity
import android.app.AlertDialog.Builder
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.DialogFragment
import android.util.Log
import android.widget.Toast
import hr.zeroinfinity.R
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.DialogWithResult
import hr.zeroinfinity.models.CategoryModel
import hr.zeroinfinity.services.retrofit.FinanceAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


class NewCategoryDialogFragment : DialogFragment(), DialogWithResult<CategoryModel> {
    private lateinit var input: TextInputEditText
    private var listener: DialogResultListener<CategoryModel>? = null
    private lateinit var mActivity: Activity

    override fun setListener(_listener: DialogResultListener<CategoryModel>) {
        listener = _listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val createProjectAlert = Builder(activity)

        createProjectAlert.setTitle(getString(R.string.new_category_dialog_label))

        val inflater = activity!!.layoutInflater
        mActivity = activity!!
        val view = inflater.inflate(R.layout.fragment_dialog, null)

        input = view.findViewById(R.id.newCategory)

        createProjectAlert.setView(view)
                .setPositiveButton(getString(R.string.add_button_label)) { _, _ ->
                    if (input.text.isNullOrEmpty()) {
                        input.error = getString(R.string.required_field_error)
                    } else {
                        val categoryName = input.text?.toString()
                        val groupId = UUID.fromString(arguments?.getString("groupId"))

                        val model = CategoryModel()
                        model.groupId = groupId
                        model.name = categoryName

                        FinanceAPI.instance.registerCategory(model)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        {
                                            model.id = it.entityId
                                            listener?.onResult(model)
                                            dismiss()
                                        },
                                        {
                                            Log.e("HTTP ERROR", it.message)
                                            Toast.makeText(mActivity, mActivity.getString(R.string.server_unavailable_label), Toast.LENGTH_SHORT).show()
                                        })
                    }


                }
                .setNegativeButton(getString(R.string.cancel_button_label)) { dialog, _ ->
                    dialog.cancel()
                }

        return createProjectAlert.create()
    }
}
