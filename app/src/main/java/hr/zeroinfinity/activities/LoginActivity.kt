package hr.zeroinfinity.activities

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import hr.zeroinfinity.R
import hr.zeroinfinity.common.DialogResultListener
import hr.zeroinfinity.common.Listener
import hr.zeroinfinity.services.UserService


class LoginActivity : AppCompatActivity() {

    private lateinit var imm: InputMethodManager
    private lateinit var view: View
    private lateinit var usernameInput: EditText
    private lateinit var passwordInput: EditText
    private lateinit var loginBtn: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var registerBtn: TextView
    private lateinit var resetPasswordBtn: TextView
    private val registerActivityRequestCode: Int = 654


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)


        view = findViewById(R.id.login_view)
        usernameInput = view.findViewById(R.id.email_input)
        passwordInput = findViewById(R.id.passwordInput)
        progressBar = findViewById(R.id.progressBar)
        loginBtn = findViewById(R.id.login_btn)
        registerBtn = findViewById(R.id.register_btn)
        resetPasswordBtn = findViewById(R.id.resetPassword_btn)

        imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        loginBtn.setOnClickListener {

            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            if (usernameInput.text.toString().isEmpty() || passwordInput.text.toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.login_entry), Toast.LENGTH_LONG).show()
            } else {
                loginBtn.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
                UserService
                        .login(usernameInput.text.toString(), passwordInput.text.toString())
                        .addListener(object : Listener<Boolean> {
                            override fun update(data: Boolean) {
                                if (data) {
                                    this@LoginActivity.runOnUiThread {
                                        Toast.makeText(this@LoginActivity, getString(R.string.login_succeed), Toast.LENGTH_SHORT).show()
                                        startActivity(Intent(this@LoginActivity, TestActivity::class.java))
                                        loginBtn.visibility = View.VISIBLE
                                        progressBar.visibility = View.GONE
                                    }
                                } else {
                                    this@LoginActivity.runOnUiThread {
                                        loginBtn.visibility = View.VISIBLE
                                        progressBar.visibility = View.GONE
                                        Toast.makeText(this@LoginActivity, getString(R.string.login_failed), Toast.LENGTH_LONG).show()
                                    }
                                }
                            }

                        })
            }
        }

        registerBtn.setOnClickListener {
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            startActivityForResult(Intent(this, RegisterActivity::class.java), registerActivityRequestCode)
        }

        resetPasswordBtn.setOnClickListener {
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            val dialog = ForgotPasswordDialogFragment()
            dialog.setListener(object : DialogResultListener<Boolean> {
                override fun onResult(result: Boolean) {
                    if (result) {
                        Snackbar.make(this@LoginActivity.view,
                                getString(R.string.new_password_warning_label),
                                Snackbar.LENGTH_INDEFINITE)
                                .setAction(getString(R.string.check_button_label)) {
                                    openMailClient();
                                }.show()
                    }
                }
            })

            dialog.show(supportFragmentManager, "recover_password")
        }

        view.setOnClickListener {
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == registerActivityRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                Snackbar.make(this@LoginActivity.view,
                        getString(R.string.registered_successfuly_message),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.check_button_label)) {
                            openMailClient()
                        }.show()
            }
        }
    }

    override fun onBackPressed() {
        if (!imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)) {
            AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.exit))
                    .setMessage(getString(R.string.exit_question))
                    .setPositiveButton(getString(R.string.yes)) { _, _ -> moveTaskToBack(true) }
                    .setNegativeButton(getString(R.string.no), null)
                    .show()
        }
    }

    private fun openMailClient() {
        val emailLauncher = Intent(Intent.ACTION_VIEW)
        emailLauncher.type = "message/rfc822"
        try {
            val emailClientNames = ArrayList<String>()
            val emailClientPackageNames = ArrayList<String>()
            // finding list of email clients that support send email
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "abc@gmail.com", null))
            val pkgManager = this@LoginActivity.packageManager
            val packages = pkgManager.queryIntentActivities(intent, 0)
            if (!packages.isEmpty()) {
                for (resolveInfo in packages) {
                    // finding the package name
                    val packageName = resolveInfo.activityInfo.packageName
                    emailClientNames.add(resolveInfo.loadLabel(packageManager).toString())
                    emailClientPackageNames.add(packageName)
                }
                // a selection dialog  for the email clients
                val builder = AlertDialog.Builder(this@LoginActivity)
                builder.setTitle(getString(R.string.choose_label))
                builder.setItems(emailClientNames.toTypedArray()) { _, which ->
                    // on click we launch the right package
                    val intent = packageManager.getLaunchIntentForPackage(emailClientPackageNames[which])
                    startActivity(intent)
                }
                val dialog = builder.create()
                dialog.show()
                return
            } else {

            }
        } catch (e: ActivityNotFoundException) {
        }

        Toast.makeText(
                this@LoginActivity,
                getString(R.string.mail_client_error),
                Toast.LENGTH_SHORT).show()
    }
}
