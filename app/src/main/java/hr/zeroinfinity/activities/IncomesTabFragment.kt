package hr.zeroinfinity.activities

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.IncomesListAdapter
import hr.zeroinfinity.common.NetworkState
import hr.zeroinfinity.common.NpaGridLayoutManager
import hr.zeroinfinity.common.RequestFailure
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.models.IncomeModel
import hr.zeroinfinity.services.datasources.IncomeDataSource
import hr.zeroinfinity.services.executor.MainThreadExecutor
import hr.zeroinfinity.services.factories.IncomeDataSourceFactory
import hr.zeroinfinity.services.retrofit.FinanceAPI
import java.util.*
import java.util.concurrent.Executor

class IncomesTabFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private var incomesListAdapter: IncomesListAdapter? = IncomesListAdapter(null)
    private var incomesListViewHeader: IncomesListAdapter.IncomesListViewHeader? = null
    private lateinit var recyclerView: RecyclerView
    private var executor: Executor = MainThreadExecutor()
    private var dataSourceFactory: IncomeDataSourceFactory? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private lateinit var progressBar: ProgressBar
    private lateinit var list: LiveData<PagedList<IncomeModel>>

    companion object {
        const val registerIncomeRequestCode = 234
        const val updateIncomeRequestCode = 1234
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        setGroup(arguments?.getParcelable("group")!!)
        return inflater.inflate(R.layout.fragment_incomes_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecycleView(view)
        incomesListViewHeader?.fetchData()
    }

    private fun initRecycleView(_view: View) {
        recyclerView = _view.findViewById(R.id.recycler_view_income)!!
        swipeRefreshLayout = _view.findViewById(R.id.swipe_container_incomes)
        progressBar = _view.findViewById(R.id.progress_bar_list)

        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = NpaGridLayoutManager(_view.context, 1)
            recyclerView.setHasFixedSize(false)
            recyclerView.adapter = incomesListAdapter

            incomesListViewHeader = incomesListAdapter?.IncomesListViewHeader(_view)!!
            incomesListViewHeader?.bindTo()

            swipeRefreshLayout?.setOnRefreshListener(this)
        }
    }

    private fun setGroup(selectedGroup: GroupModel) {
        incomesListAdapter?.group = selectedGroup

        initDataSource(selectedGroup.id!!)
    }

    private fun initDataSource(groupId: UUID?) {
        dataSourceFactory = IncomeDataSourceFactory(FinanceAPI.instance, groupId!!)
        dataSourceFactory?.postLiveData?.observeForever(Observer<IncomeDataSource> {
            it?.getRequestFailureLiveData()?.observeForever(Observer<RequestFailure> { requestFailure ->
                if (requestFailure == null) return@Observer

                Snackbar.make(this@IncomesTabFragment.view!!,
                        getString(R.string.server_unavailable_label),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry_button_label)) {
                            requestFailure.retryable.retry()
                        }.show()
            })

            it?.getNetworkStateLiveData()?.observeForever { it1 ->
                progressBar.visibility = if(it1 == NetworkState.LOADING) View.VISIBLE else View.INVISIBLE
            }
        })

        val config = PagedList.Config.Builder()
                .setPageSize(FinanceAPI.DEFAULT_PER_PAGE)
                .setInitialLoadSizeHint(FinanceAPI.DEFAULT_PER_PAGE * 2)
                .setEnablePlaceholders(true)
                .build()

        list = LivePagedListBuilder(dataSourceFactory!!, config)
                .setFetchExecutor(executor)
                .build()

        incomesListAdapter?.submitList(list.value)
        list.observeForever {
            incomesListAdapter?.submitList(it)
        }
    }

    override fun onRefresh() {
        incomesListViewHeader?.fetchData()
        dataSourceFactory?.postLiveData?.value?.invalidate()
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun onPause() {
        swipeRefreshLayout?.isRefreshing = false
        super.onPause()
    }
}


