package hr.zeroinfinity.activities

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import hr.zeroinfinity.R
import hr.zeroinfinity.adapters.GoalsListAdapter
import hr.zeroinfinity.common.NetworkState
import hr.zeroinfinity.common.NpaGridLayoutManager
import hr.zeroinfinity.common.RequestFailure
import hr.zeroinfinity.models.GoalModel
import hr.zeroinfinity.models.GroupModel
import hr.zeroinfinity.services.datasources.GoalDataSource
import hr.zeroinfinity.services.executor.MainThreadExecutor
import hr.zeroinfinity.services.factories.GoalDataSourceFactory
import hr.zeroinfinity.services.retrofit.FinanceAPI
import java.util.*
import java.util.concurrent.Executor
import java.util.function.Consumer


class GoalsTabFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private var goalsListAdapter: GoalsListAdapter? = GoalsListAdapter(null, Consumer { onRefresh() })
    private var goalListViewHeader: GoalsListAdapter.GoalsListViewHeader? = null
    private lateinit var recyclerView: RecyclerView
    private var executor: Executor = MainThreadExecutor()
    private var dataSourceFactory: GoalDataSourceFactory? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private lateinit var progressBar: ProgressBar
    private lateinit var list: LiveData<PagedList<GoalModel>>

    companion object {
        const val registerGoalRequestCode = 12332
        const val updateGoalRequestCode = 23432
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        setGroup(arguments?.getParcelable("group")!!)
        return inflater.inflate(R.layout.fragment_goals_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecycleView(view)
        goalListViewHeader?.fetchData()
    }

    private fun initRecycleView(_view: View) {
        recyclerView = _view.findViewById(R.id.recycler_view_goal)!!
        swipeRefreshLayout = _view.findViewById(R.id.swipe_container_goals)
        progressBar = _view.findViewById(R.id.progress_bar_list)

        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = NpaGridLayoutManager(_view.context, 1)
            recyclerView.setHasFixedSize(false)
            recyclerView.adapter = goalsListAdapter
            goalListViewHeader = goalsListAdapter?.GoalsListViewHeader(_view)
            goalListViewHeader?.bindTo()

            swipeRefreshLayout?.setOnRefreshListener(this)
        }
    }

    private fun setGroup(selectedGroup: GroupModel) {
        goalsListAdapter?.group = selectedGroup
        initDataSource(selectedGroup.id!!)
    }

    private fun initDataSource(groupId: UUID?) {
        dataSourceFactory = GoalDataSourceFactory(FinanceAPI.instance, groupId!!)
        dataSourceFactory?.postLiveData?.observeForever(Observer<GoalDataSource> {
            it?.getRequestFailureLiveData()?.observeForever(Observer<RequestFailure> { requestFailure ->
                if (requestFailure == null) return@Observer

                Snackbar.make(this@GoalsTabFragment.view!!,
                        getString(R.string.server_unavailable_label),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry_button_label)) {
                            requestFailure.retryable.retry()
                        }.show()
            })

            it?.getNetworkStateLiveData()?.observeForever { it1 ->
                progressBar.visibility = if(it1 == NetworkState.LOADING) View.VISIBLE else View.INVISIBLE
            }
        })

        val config = PagedList.Config.Builder()
                .setPageSize(FinanceAPI.DEFAULT_PER_PAGE)
                .setInitialLoadSizeHint(FinanceAPI.DEFAULT_PER_PAGE * 2)
                .setEnablePlaceholders(true)
                .build()

        list = LivePagedListBuilder(dataSourceFactory!!, config)
                .setFetchExecutor(executor)
                .build()

        goalsListAdapter?.submitList(list.value)
        list.observeForever {
            goalsListAdapter?.submitList(it)
        }
    }

    override fun onRefresh() {
        swipeRefreshLayout?.isRefreshing = false
        goalListViewHeader?.fetchData()
        dataSourceFactory?.postLiveData?.value?.invalidate()
    }

    override fun onPause() {
        swipeRefreshLayout?.isRefreshing = false
        super.onPause()
    }
}


